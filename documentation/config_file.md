# Apigen configuration file

The path to the configuration file can be given with the `-c` CLI parameter.
If not given, the program will check if a file `apigen.ini` exists and read that.

The file must have the [ini](https://docs.fileformat.com/system/ini/) file format,
i.e. define simple key-value pairs in sections.
There are are standard sections: *validation* and *logging* and optional sections for each
generator (e.g. *html*).

The parameters given here can also be given directly on the command line.
For the generator specific variables this happens via the `-C key=value` parameter.

## Example

```
[validation]

# equivalent to the --validate-references CLI toggle
references = 1

# equivalent to the --validate-array-order CLI toggle
array_order = 1

# equivalent to the --validate-ignore CLI toggles
ignore =


[logging]

# equivalent to the -v/--verbosity=DEBUG CLI parameter
loglevel = DEBUG

# equivalent to redirecting stderr file descriptor in the shell
logfile = path/to/logfile.log
# Use "-" for stdout and "+" for stderr (the default)


[html]
# Parameters given directly to the html generator

# Equivalent to the -C style=mine CLI parameter
style = mine
```
