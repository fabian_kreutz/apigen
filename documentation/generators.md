# Apigen Generators

These generators are included in the apigen python package.

## HTML generator

The html generator outputs an openapi or asyncapi specification as html documentation
using templates defined here.
It can be configured with these variables:

| Key | Default | Description |
| --- | ------- | ----------- |
| style | basic | Selects one of the template directories that come with apigen |
| template_dir | | Adds a template directory anywhere |
| rc.\* | | Adds a value to the template rendering context (See below) |

### Template selection

The renderer will select the file `index.html.jinja` from the template directories.
The search path for templates is

1. An optionally added *template_dir* anywhere
1. The style directory in `apigen/generators/templates/html/{style}`
1. The common directory `apigen/generators/templates/html/common`

Currently supported styles:

* basic

### Render context

Additionally to the main *spec* variable given to the templates, more context is given and
can be added to.

| Variable | Default | Description |
| -------- | ------- | ----------- |
| generator_version | version of the apigen package | Used in common/index.html.jinja |
| date_created | An ISO formatted today | Used in common/index.html.jinja |
| base_url | / | Used in basic/index.html.jinja |

When defining own templates, the rendering context must be added via a generator variable
that starts with `rc.`.
For example the CLI parameter `-C rc.base_url=/path/to/doc` will set the *base_url* context
variable.

## JSON/YAML specification generator

This generator is not a big deal, as the specification is internally stored in a very similar
structure.  It simply renders that as JSON or YAML.
