# The catalog

## Overview

Both AsyncAPI and OpenAPI define a root element "components" which can contained named objects
that are reusable via reference by objects in the "normal" structure.
This feature can be used to name responses, reuse them from different places or even
combine them using the JSONSchema _allOf_ combinator or the AsynAPI _traits_ feature.

```json
{
  "components": {
    "schemas": {
      "MyNamedSchema": { ... }
    }
  }
  "paths": {
    ...
    "content": {"$ref": "#/components/schemas/MyNamedSchema"}
  }
}
```

The catalog however presents some challenges.
Firstly, the pydantic parsing is context-free, i.e. when an object is created from the
dictionary, it is unaware of parent elements.
Secondly, without context awareness, we cannot recognize recursive specification, where
e.g. a schema references itself, or a child references its parent.

## Current solution

The top-level objects (the ApiSpecs) will create a catalog and traverse the data before parsing
to replace each reference with a reference object that has access to the catalog.
While parsing, these references are in turn evaluated, while keeping track of previously
evaluated references in order to break recursion.

This means that if you want to parse only a partial tree (not starting from the root ApiSpec),
then you need to create the catalog and inject it in the way the ApiSpecs do it.

### AsyncApi traits

The _traits_ feature of AsyncAPI complicates the parsing considerably.

Any object that can have traits needs to merge their data into the base before construction.
However if a reference results in more traits being added, then the trait handling happens
earlier than the catalog substitution.

This order in the MRO:

```python
class MergesTraits:
    def __init__(self, **values):
        if traits in values:
            values = merge_traits
        super().__init__(**values)

class MessageTraitObject(BaseModel):
    messageId: str
    ...

class MessageObject(MergesTraits, MessageTraitObject):
    traits: List[MessageTraitObject]
```

results in traits not yet known to MergesTraits, because BaseModel resolves the reference too late.
But the order

```
class MessageObject(MessageTraitObject, MergesTraits):
    ...
```

does not work, because the pydantic BaseModel's constructor does not call super.

So each subclass of any model needs to its own reference resolution.
