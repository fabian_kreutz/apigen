# API documentation generator

This program can deal with [OpenAPI](https://swagger.io/specification/) V3.0.\* and
[AsyncAPI](https://www.asyncapi.com) V2.3.\*/2.4.\* specifications through pluggable
parsers and generators.

The subcommand `generate` will read an input file and render the derived spec into another file.

The subcommand `validate` will read two input files and compare the specs derived from them.

## CLI usage

Install the package and call the `apigen` executable.

Detailed syntax of the call can be seen when using the `--help/-h` parameter.
CLI parameters can also be given in a [configuration file](config_file.md).

### Generation

```
$ apigen \
    generate \
    path/to/input_file.yaml \
    -l html
```

The output file location can be given or it will be deduced from the input filename and the default output suffix of the [chosen output language](generators.md).

Some generators require or allow additional context variables, which can be given either
using the `-C` parameter or in a section of the *configuration file* bearing the
name of the generator language (e.g. "html").

### Validation

```
$ apigen \
    validate \
    path/to/first_spec.json \
    path/to/second_spec.yaml \
    --validate-references
```

The input files can be of different formats, as long as apigen finds a parser that can derive a
specification from it.

Possible validation flags are

| Flag | Default | Description |
| ---- | ------- | ----------- |
| validate-references | False | If false, all references will be expanded before comparing; if true, even a different name for a component will invalidate the spec |
| validate-array-order | False | If false, parameters will be sorted by name before comparing |
| validate-ignore      | empty list | When the validator enters a listed branch, it will ignore any difference there |

Validation options can be given in the *configuration file*:

```
[validation]
references = 1
array_order = 0
ignore =
  /servers
  /info/license
```

## Functional interface

Parts of the total process (find configuration, read and validate spec, generate output) can be
executed using the functional interface.
The mechanism through which references are resolved is further described on [the Catalog](catalog.md).

### Parsing of the specification

Parsing a specification file or an object structure can be done with these functions:

```py
from pathlib import Path
from apigen.spec import ApiSpec
from apigen.parsers.spec import SpecParser

parser = SpecParser()

spec = parser.parse(Path("path/to/file.yaml"))
assert isinstance(spec, ApiSpec)
spec = parser.parse_structure({"openapi": "3.0.3", ...})
```

The ApiSpec is a complex structure of [dataclasses](https://docs.python.org/3/library/dataclasses.html)
(actually [pydantic](https://pydantic-docs.helpmanual.io) basemodels).

### Finding differences in two specifications

```py
from apigen.differ import SpecDiffer

differ = SpecDiffer(validate_references=False)

assert differ.diff(spec1, spec1) == []
for difference in differ.diff(spec1, spec2):
    print(difference)
```

## Custom parsers and generators

The apigen cli interface discovers possible parsers and generators via entry-points:

```
# setup.cfg
[options.entry_points]
apigen_parsers =
    spec = apigen.parsers.spec:SpecParser
apigen_generators =
    html = apigen.generators.html:HtmlGenerator
```

This package itself only contains
 * a parser for json and yaml specification files,
 * a generator for the same specification formats,
 * a generator for HTML templates (using Jinja2).

A parser and generator package for python code (using django) is under work.
Send a message, if you have created your own package.

### Generating output

Generators derive from `apigen.generators.BaseGenerator` and must define a standard target suffix, a `render` function and a constructor that takes arbitrary keyword arguments:

```
from apigen.generators import BaseGenerator
from apigen.spec import ApiSpec

class HtmlGenerator(BaseGenerator):
    target_suffix = "html"

    def __init__(self, **kwargs):
        style = kwargs.get("style", "default_value")
        ...

    def render(self, spec: ApiSpec) -> str:
        ...
```

The name that becomes available for the cli parameter `-l/--language` is the key name of the entry-point in the `setup.cfg`.
That same name can also be used as section name in the configuration file.
Values in that file will be given to the generator classes constructor.

### Parsing source files

Parsers can be added as dynamically as generators.
They must decide if they can parse a file (usually by suffix) and provide a function to parse it:

```
from pathlib import Path
from apigen.parsers import BaseParser
from apigen.spec import ApiSpec

class SpecParser(BaseParser):
    @classmethod
    def can_parse(cls, path: Path):
        return path.suffix in [".json", ".yaml", ".yml"]

    def parse(self, path: Path) -> ApiSpec:
        ...
```
