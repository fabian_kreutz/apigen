import logging
import sys


#: global logger instance
logger: logging.Logger = logging.getLogger(__package__)
verbosity_to_loglevel = [
    'NOTSET', 'CRITICAL', 'ERROR', 'WARNING', 'INFO', 'DEBUG']


def configure(verbosity=None, loglevel='INFO', logfile='+'):
    if verbosity is not None:
        if verbosity.isdigit():
            loglevel = verbosity_to_loglevel[int(verbosity)]
        elif len(verbosity) == 1:
            loglevel = [
                val for val in verbosity_to_loglevel
                if val[0] == verbosity][0]
        else:
            loglevel = verbosity
    logger.setLevel(loglevel)
    logger.handlers.clear()
    if logfile == '-':
        logger.addHandler(logging.StreamHandler(sys.stdout))
    elif logfile == '+':
        logger.addHandler(logging.StreamHandler(sys.stderr))
    else:
        try:
            logger.addHandler(logging.FileHandler(logfile))
        except OSError as oe:
            logger.addHandler(logging.StreamHandler(sys.stderr))
            logger.error(str(oe))


configure()
