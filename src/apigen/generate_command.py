import argparse
from pathlib import Path
from .config import Subcommand, Config
from .exceptions import ApigenError
from .generators import BaseGenerator
from .utils import extract_components
from .registry import registry, parse_spec


class Generate(Subcommand):
    '''
    Render the spec derived from the input file in another format
    '''
    input_file: Path
    output_file: Path
    generator_class: type[BaseGenerator]
    generator_config: Config
    extract = False

    @classmethod
    def add_arguments(cls, sp):
        sp.add_argument(
            "input", nargs="?", default="-",
            help="Path to the file to parse (stdin by default)")
        sp.add_argument(
            "output", nargs="?",
            help="Path to output file; default: input with appropriate suffix")
        sp.add_argument(
            "-l", "--language",
            help="Name of the generator to use; default: html",
            default="html", choices=registry.generators)
        sp.add_argument(
            '-C', '--config', metavar="KEY=VALUE", action='append', default=[],
            help="Configuration values for the generator")
        sp.add_argument(
            '-E', '--extract', action="store_true", default=False,
            help="Extract response schemas into components")
        return sp

    def __init__(self, args: argparse.Namespace):
        super().__init__(args)
        self.generator_class = gc = registry.generators[args.language]
        if args.output == "-":
            args.output = "/dev/stdout"
        if args.input == "-":
            self.input_file = Path("/dev/stdin")
            self.output_file = Path(args.output or "/dev/stdout")
        elif (input_file := Path(args.input)).exists():
            self.input_file = input_file
            self.output_file = Path(
                args.output or
                self.input_file.with_suffix(f".{gc.target_suffix}"))
        else:
            raise ApigenError(f"File not found: `{args.input}`")
        if not (parent := self.output_file.absolute().parent).exists():
            if not parent.parent.exists():
                raise ApigenError(f"Output dir does not exist: `{parent}`")
            parent.mkdir()
        if args.language in self.file_config:
            self.generator_config = dict(self.file_config[args.language])
        else:
            self.generator_config = {}
        self.generator_config.update(self.parse_config_params(args.config))
        self.extract = args.extract

    def parse_config_params(self, from_args: list[str]) -> Config:
        config = {}
        for elem in from_args:
            if "=" not in elem or elem[0] == "=" or elem[-1] == "=":
                raise ApigenError(f"Invalid Key=Value pair: `{elem}`")
            key, value = elem.split("=", 1)
            config[key.strip()] = value.strip()
        return config

    def run(self):
        api_spec = parse_spec(self.input_file)
        if self.extract:
            extract_components(api_spec)
        generator = self.generator_class(**self.generator_config)
        rendition = generator.render(api_spec)
        self.output_file.write_text(rendition)
