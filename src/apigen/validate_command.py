import argparse
from pathlib import Path
import sys
from .config import Subcommand, Config
from .exceptions import ApigenError
from .differ import SpecDiffer
from .registry import parse_spec


def gc(file_config, args, option, default):
    if (val := getattr(args, option)) is not None:
        return val
    return file_config.getboolean("validation", option, fallback=default)


def add_validation_toggle(parser, long_name, short_name, help1, help2):
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        f"-{short_name}", f"--validate-{long_name}", default=None,
        action="store_true", dest=long_name.replace("-", "_"), help=help1)
    group.add_argument(
        f"-{short_name.upper()}", f"--no-{long_name}", default=None,
        action="store_false", dest=long_name.replace("-", "_"), help=help2)


class Validate(Subcommand):
    '''
    Validate if a second file complies to the spec of the first
    '''
    input_file: Path
    other_file: Path
    validator_config: Config

    @classmethod
    def add_arguments(cls, sp):
        sp.add_argument(
            'input', type=Path, metavar="spec",
            help="Path to the file that defines the spec")
        sp.add_argument(
            'other', type=Path,
            help="Path to file that should be validated")
        add_validation_toggle(
            sp, "references", "r",
            help1="Ensure that both specs define and use the same references",
            help2="Default is to ignore referencing differences")
        add_validation_toggle(
            sp, "array-order", "a",
            help1="Ensure that parameter array order is the same",
            help2="Default is to sort parameters before comparing")
        sp.add_argument(
            "-I", "--validate-ignore", action="append", default=None,
            dest="ignore",
            help="Path to elements that should not be validated. "
            "Can be given multiple times.")

    def __init__(self, args: argparse.Namespace):
        super().__init__(args)
        if not args.input.exists():
            raise ApigenError(f"File not found: `{args.input}`")
        if not args.other.exists():
            raise ApigenError(f"File not found: `{args.other}`")
        self.input_file = args.input
        self.other_file = args.other
        self.validator_config = {
            "validate_references":
                gc(self.file_config, args, "references", False),
            "validate_array_order":
                gc(self.file_config, args, "array_order", False),
            "validate_ignore": self.merge_ignores(args),
        }

    def merge_ignores(self, args):
        result = getattr(args, "ignore") or []
        from_file = self.file_config.get("validation", "ignore", fallback="")
        result.extend(
            entry.strip() for entry in from_file.split("\n") if entry.strip())
        return result

    def run(self):
        spec1 = parse_spec(self.input_file)
        spec2 = parse_spec(self.other_file)
        differences = SpecDiffer(**self.validator_config).diff(spec1, spec2)
        if not differences:
            sys.exit(0)
        for entry in differences:
            print(entry)
        sys.exit(1)
