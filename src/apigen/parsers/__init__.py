import abc
from pathlib import Path
from ..spec import ApiSpec


class BaseParser(metaclass=abc.ABCMeta):
    @classmethod
    @abc.abstractmethod
    def can_parse(cls, path: Path) -> bool:
        raise NotImplementedError()

    @abc.abstractmethod
    def parse(self, path: Path) -> ApiSpec:
        raise NotImplementedError()
