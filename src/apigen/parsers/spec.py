import json
import yaml
from . import BaseParser
from ..spec import OpenApiV3Spec, AsyncApiV2Spec
from ..exceptions import ApigenError


class SpecParser(BaseParser):
    @classmethod
    def can_parse(cls, path):
        return path.suffix in [".json", ".yaml", ".yml"]

    def parse(self, path):
        match path.suffix:
            case ".json":
                reader = json.loads
            case ".yaml" | ".yml":
                reader = yaml.safe_load
            case _:
                if path.open("rt").read(1) == "{":
                    reader = json.loads
                else:
                    reader = yaml.safe_load
        try:
            structure = reader(path.read_text())
            return self.parse_structure(structure)
        except Exception as exc:
            raise ApigenError(f"Unable to parse input file `{path}`: {exc}")

    def parse_structure(self, structure):
        if not isinstance(structure, dict):
            raise ValueError("File does not contain a structure")
        if "openapi" in structure:
            version = structure["api_version"] = structure.pop("openapi")
            match [int(comp) for comp in version.split(".")]:
                case [3, 0, _]:
                    return OpenApiV3Spec(**structure)
                case _:
                    raise ValueError(f"Unsupported openapi version {version}")
        elif "asyncapi" in structure:
            version = structure["api_version"] = structure.pop("asyncapi")
            match [int(comp) for comp in version.split(".")]:
                case [2, 3, _] | [2, 4, _]:
                    return AsyncApiV2Spec(**structure)
                case _:
                    raise ValueError(f"Unsupported asyncapi version {version}")
        elif "swagger" in structure:
            raise ValueError("File contains outdated `swagger` format")
        raise ValueError("File does not seem to contain an API definition")
