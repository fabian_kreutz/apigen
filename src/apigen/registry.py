from importlib.metadata import entry_points
from pathlib import Path
import sys
from .exceptions import ApigenError
from .generators import BaseGenerator
from .parsers import BaseParser


class Registry:
    _generators: dict[str, type[BaseGenerator]] = {}
    _parsers: dict[str, type[BaseParser]] = {}

    @property
    def generators(self):
        if not (gens := getattr(self, "_generators")):
            gens = self._generators = self.find_entrypoints("generators")
        return gens

    @property
    def parsers(self):
        if not (pars := getattr(self, "_parsers")):
            pars = self._parsers = self.find_entrypoints("parsers")
        return pars

    @staticmethod
    def find_entrypoints(type):
        generators = {}
        for ep in entry_points(group=f"apigen_{type}"):
            try:
                generators[ep.name] = ep.load()
            except Exception as exc:
                sys.stderr.write(
                    f"Problem loading class {ep.name}: {exc}\n")
                sys.exit(2)
        return generators


#: global list of known generators
registry = Registry()


def parse_spec(file: Path):
    for parser_class in registry.parsers.values():
        if parser_class.can_parse(file):
            return parser_class().parse(file)
    raise ApigenError(f"No parser found for file `{file}`")
