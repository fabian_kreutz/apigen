import enum
from typing import Optional, Any, Dict, List, Union
import pydantic
from .definitions import lf, df, cf, BaseModel, url_validator, json_merge
from .common import ApiSpec, make_security_scheme_class, get_oauth_scopes
from .openapi import (
    InfoObject, TagObject, ServerVariableObject, ExternalDocumentationObject,
    SecurityRequirementObject, SchemaObject,
    ServerObject as OpenApiServerObject)
from .bindings import (
    ServerBindingsObject, ChannelBindingsObject,
    OperationBindingsObject, MessageBindingsObject)


def merge_traits(base, *traits):
    for trait in traits:
        if (reference := trait.get("$ref", None)) is not None:
            trait = reference.get_dict(False)
        base = json_merge(base, trait)
    return base


class SecurityScheme(enum.Enum):
    userPassword = "userPassword"
    apiKey = "apiKey"
    X509 = "X509"
    symmetricEncryption = "symmetricEncryption"
    asymmetricEncryption = "asymmetricEncryption"
    httpApiKey = "httpApiKey"
    http = "http"
    oauth2 = "oauth2"
    openIdConnect = "openIdConnect"
    plain = "plain"
    scramSha256 = "scramSha256"
    scramSha512 = "scramSha512"
    gssapi = "gssapi"


class HTTPMethod(enum.Enum):
    get = "GET"
    post = "POST"
    put = "PUT"
    patch = "PATCH"
    delete = "DELETE"
    head = "HEAD"
    options = "OPTIONS"
    connect = "CONNECT"
    trace = "TRACE"


SchemaObjectOrEmpty = Union[bool, SchemaObject]
SecuritySchemeObject = make_security_scheme_class(SecurityScheme)


class ParameterObject(BaseModel):
    schema_: SchemaObjectOrEmpty = pydantic.Field(alias="schema")
    description: Optional[str]
    location: Optional[str]


# ServerVariableObject also has "examples"
class ServerObject(OpenApiServerObject):
    protocol: str
    protocol_version: Optional[str]
    security: List[SecurityRequirementObject] = lf
    bindings: ServerBindingsObject = cf(ServerBindingsObject)


class CorrelationIdObject(BaseModel):
    location: str
    description: Optional[str]


class MessageExampleObject(BaseModel):
    headers: Dict[str, Any]
    payload: Any
    name: Optional[str]
    summary: Optional[str]


class MergesTraits:
    def __init__(self, **values):
        if (reference := values.get("$ref", None)) is not None:
            values = reference.get_dict()
        if (traits := values.get("traits")) is not None:
            values = merge_traits(values, *traits)
        super().__init__(**values)


class MessageTraitObject(BaseModel):
    messageId: Optional[str]
    headers: Optional[SchemaObjectOrEmpty]
    correlationId: Optional[CorrelationIdObject]
    schemaFormat: str = "application/vnd.aai.asyncapi;version=2.4.0"
    contentType: Optional[str]
    name: Optional[str]
    title: Optional[str]
    summary: Optional[str]
    description: Optional[str]
    tags: List[TagObject] = lf
    externalDocs: Optional[ExternalDocumentationObject]
    bindings: MessageBindingsObject = cf(MessageBindingsObject)
    examples: List[MessageExampleObject] = lf

    @property
    def has_visible_headers(self):
        return isinstance(self.headers, SchemaObject)


class MessageObject(MergesTraits, MessageTraitObject):
    payload: Union[SchemaObject, Any]
    traits: List[MessageTraitObject] = lf

    @property
    def is_schema(self):
        return isinstance(self.payload, SchemaObject)


class OperationTraitObject(BaseModel):
    operationId: Optional[str]
    summary: Optional[str]
    description: Optional[str]
    security: List[SecurityRequirementObject] = lf
    tags: List[TagObject] = lf
    externalDocs: Optional[ExternalDocumentationObject]
    bindings: OperationBindingsObject = cf(OperationBindingsObject)


class OperationObject(MergesTraits, OperationTraitObject):
    message: Union[MessageObject, Dict[str, List[MessageObject]]]
    traits: List[OperationTraitObject] = lf

    oauth_scopes = get_oauth_scopes

    @pydantic.validator("message", allow_reuse=True)
    def only_oneof_combination_allowed(cls, val, values, **kwargs):
        if isinstance(val, dict):
            for key in val:
                if key != "oneOf":
                    raise ValueError(
                        f"Only oneOf combination allowed, not {key}")
        return val

    @property
    def oneOf(self):
        return isinstance(self.message, dict)

    @property
    def messages(self):
        if self.oneOf:
            return self.message["oneOf"]
        else:
            return [self.message]


class ChannelItemObject(BaseModel):
    description: Optional[str]
    servers: List[str] = lf    # could validate that these exist in components
    subscribe: Optional[OperationObject]
    publish: Optional[OperationObject]
    parameters: Dict[str, ParameterObject] = df
    bindings: ChannelBindingsObject = cf(ChannelBindingsObject)

    @property
    def operations(self):
        return {
            name: value
            for name in ["subscribe", "publish"]
            if (value := getattr(self, name)) is not None}


class ComponentsObject(BaseModel):
    schemas: Dict[str, SchemaObjectOrEmpty] = df
    servers: Dict[str, ServerObject] = df
    serverVariables: Dict[str, ServerVariableObject] = df
    channels: Dict[str, ChannelItemObject] = df
    messages: Dict[str, MessageObject] = df
    securitySchemes: Dict[str, SecuritySchemeObject] = df  # type: ignore
    parameters: Dict[str, ParameterObject] = df
    correlationIds: Dict[str, CorrelationIdObject] = df
    operationTraits: Dict[str, OperationTraitObject] = df
    messageTraits: Dict[str, MessageTraitObject] = df
    serverBindings: Dict[str, ServerBindingsObject] = df
    channelBindings: Dict[str, ChannelBindingsObject] = df
    operationBindings: Dict[str, OperationBindingsObject] = df
    messageBindings: Dict[str, MessageBindingsObject] = df


class AsyncApiV2Spec(BaseModel, ApiSpec):
    type = "asyncapi"
    id: Optional[str]
    info: InfoObject
    channels: Dict[str, ChannelItemObject] = df
    servers: Dict[str, ServerObject] = df
    defaultContentType: str = "application/json"
    components: ComponentsObject = cf(ComponentsObject)
    tags: List[TagObject] = lf
    externalDocs: Optional[ExternalDocumentationObject]

    @property
    def endpoints(self):
        return self.channels

    _validate_url = url_validator("id")
