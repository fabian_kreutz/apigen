import enum
from typing import Any, Dict, Optional, Type
import pydantic
from .definitions import BaseModel


class OAuthFlow(enum.Enum):
    implicit = "implicit"
    password = "password"
    clientCredentials = "clientCredentials"
    authorizationCode = "authorizationCode"


class NamedReference:
    def __init__(self, catalog, ref_name):
        self.catalog = catalog
        self.name = ref_name

    def get_dict(self, add_reference_name=True):
        return self.catalog.resolve(self.name, add_reference_name)

    def get_object(self):
        return self.catalog.objects.get(self.name, None)

    def set_object(self, obj):
        self.catalog.objects[self.name] = obj

    def is_initialized(self):
        return self.name in self.catalog.initialized

    def mark_initialized(self):
        self.catalog.initialized.append(self.name)


class Catalog:
    components: dict[str, Any]
    initialized: list[str]
    objects: dict[str, Any]

    def __init__(self, components):
        self.components = {"components": self.inject_self(components)}
        self.initialized = []
        self.objects = {}

    def inject_self(self, kwargs):
        result = {}
        for key, val in kwargs.items():
            if isinstance(val, dict):
                val = self.inject_self(val)
            elif isinstance(val, list) and val and isinstance(val[0], dict):
                val = [self.inject_self(elem) for elem in val]
            result[key] = val
        if (ref_name := result.get("$ref")) is not None:
            result["$ref"] = NamedReference(self, ref_name)
        return result

    def resolve(self, ref_name, add_reference_name):
        path = ref_name.split("/")
        if path[0] != "#":
            raise ValueError("Only internal references supported")
        if len(path) < 4:
            raise ValueError(f"Unsupported reference {ref_name}")
        target = self.components
        for elem in path[1:]:
            if elem not in target:
                raise ValueError(f"Undefined reference {ref_name}")
            target = target.get(elem)
        values = target.copy()
        if add_reference_name:
            values["referenceName"] = "/".join(path[2:])
        return values


class ApiSpec(pydantic.BaseModel):
    type: str
    api_version: str

    def __init__(self, **kwargs):
        catalog = Catalog(kwargs.get("components", {}))
        kwargs = catalog.inject_self(kwargs)
        super().__init__(**kwargs)

    def dict(self, **kwargs):
        result = super().dict(**kwargs)
        result[result.pop("type")] = result.pop("api_version")
        return result

    def deepcopy(self):
        data = self.dict()
        return type(self)(
            api_version=data.pop(self.type),
            **data)

    @property
    def acronym(self):
        return ''.join(ch for ch in self.info.title if ch.isupper())


class OAuthFlowObject(BaseModel):
    scopes: Dict[str, str]
    authorizationUrl: Optional[str]
    tokenUrl: Optional[str]
    refreshUrl: Optional[str]


def make_security_scheme_class(
        type_enum: Type[enum.Enum]) -> Type[BaseModel]:

    assert isinstance(type_enum, type) and issubclass(type_enum, enum.Enum), \
        f"Invalid type_enum class: {type_enum}"

    class SecuritySchemeObject(BaseModel):
        type: type_enum  # type: ignore
        description: Optional[str]

        def __new__(cls, *args, **kwargs):
            subcls = {
                "apiKey": ApiKeySecuritySchemeObject,
                "http": HttpSecuritySchemeObject,
                "oauth2": Oauth2SecuritySchemeObject,
                "openIdConnect": OpenIdConnectSecuritySchemeObject,
                }.get(kwargs.get("type"), cls)
            result = super().__new__(subcls)
            result.__init__(*args, **kwargs)
            return result

    class ApiKeySecuritySchemeObject(SecuritySchemeObject):
        name: str
        in_: str = pydantic.Field(alias="in")

    class HttpSecuritySchemeObject(SecuritySchemeObject):
        scheme: str
        bearerFormat: Optional[str]

    class Oauth2SecuritySchemeObject(SecuritySchemeObject):
        flows: Dict[OAuthFlow, OAuthFlowObject]

        @pydantic.validator("flows", pre=True, allow_reuse=True)
        def check_required_values_for_certain_flows(cls, val):
            for flowtype, data in val.items():
                required = ["scopes"]
                match flowtype:
                    case "password":
                        required += ["tokenUrl"]
                    case "implicit":
                        required += ["authorizationUrl"]
                    case "clientCredentials":
                        required += ["tokenUrl"]
                    case "authorizationCode":
                        required += ["tokenUrl", "authorizationUrl"]
                for field in required:
                    if not data.get(field):
                        raise ValueError(
                            f"-> {flowtype} -> {field} is required")
            return val

        def dict(self, **kwargs):
            # Need to serialize flow keys (enums) to strings for json
            result = super().dict(**kwargs)
            result["flows"] = {
                key.value: value
                for key, value in result["flows"].items()}
            return result

    class OpenIdConnectSecuritySchemeObject(SecuritySchemeObject):
        openIdConnectUrl: str

    return SecuritySchemeObject


def get_oauth_scopes(operation, security_schemes):
    result = {}
    for requirement in operation.security:
        for scheme_name, required_scopes in requirement.items():
            if (scheme := security_schemes.get(scheme_name)) is None:
                raise ValueError(
                    "Operation requires unknown "
                    f"security scheme {scheme_name}")
            if scheme.type.value == "oauth2":
                for flow_type, flows in scheme.flows.items():
                    result.update({
                        key: value
                        for key, value in flows.scopes.items()
                        if key in required_scopes})
    return result
