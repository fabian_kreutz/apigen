import base64
from datetime import datetime as DateTime, timedelta as TimeDelta
import enum
import json
import re
from typing import Optional, Any, Dict, List, Union
import pydantic
from .definitions import lf, df, cf, BaseModel, url_validator
from .common import ApiSpec, make_security_scheme_class, get_oauth_scopes


timespec_re = re.compile("([0-9]+) (minutes?|hours?|days?|months?|years?) ago")


def b64encode(text):
    return base64.b64encode(text.encode()).decode()


def merge_schemas(schemas):
    result = {}
    for elem in schemas:
        if "$ref" in elem:
            elem = elem["$ref"].get_dict()
        if "not" in elem:
            continue
        if "type" not in elem:  # another combinator
            raise ValueError("too complicated")
        if elem["type"] != result.get("type", elem["type"]):
            raise ValueError("allOf with conflicting types")
        if elem["type"] == "object":
            properties = result.get("properties", {})
            properties.update(elem["properties"])
            required = result.get("required", [])
            required.extend(elem.get("required", []))
            result.update(elem)
            result["properties"] = properties
            result["required"] = required
        else:
            result.update(elem)
    return SchemaObject(**result)


class Style(enum.Enum):
    simple = "simple"
    form = "form"


class SecurityScheme(enum.Enum):
    apiKey = "apiKey"
    http = "http"
    oauth2 = "oauth2"
    openIdConnect = "openIdConnect"


class ParameterLocation(enum.Enum):
    path = "path"
    header = "header"
    query = "query"
    cookie = "cookie"


class Format(enum.Enum):
    integer_int32 = "int32"
    integer_int64 = "int64"
    number_float = "float"
    number_double = "double"
    string_byte = "byte"
    string_binary = "binary"
    string_date = "date"
    string_date_time = "date-time"
    string_password = "password"
    string_uuid = "uuid"
    string_regex = "regex"


class ExampleObject(BaseModel):
    summary: Optional[str]
    description: Optional[str]
    value: Any
    externalValue: Optional[str]


class ExternalDocumentationObject(BaseModel):
    url: str
    description: Optional[str]


class TagObject(BaseModel):
    name: str
    description: Optional[str] = None
    externalDocs: Optional[ExternalDocumentationObject] = None


class ContactObject(BaseModel):
    name: Optional[str]
    url: Optional[str]
    email: Optional[str]     # validate: email

    _validate_url = url_validator()


class LicenseObject(BaseModel):
    name: str
    url: str

    _validate_url = url_validator()

    def __init__(self, name, url=None, **kwargs):
        url = url or f"https://opensource.org/licenses/{name}"
        super().__init__(name=name, url=url, **kwargs)


class InfoObject(BaseModel):
    title: str
    version: str
    description: Optional[str]
    termsOfService: Optional[str]
    contact: Optional[ContactObject]
    license: Optional[LicenseObject]

    _validate_url = url_validator("termsOfService")


class ServerVariableObject(BaseModel):
    default: str
    description: Optional[str]
    enum: List[str] = lf


class ServerObject(BaseModel):
    url: str
    description: Optional[str]
    variables: Dict[str, ServerVariableObject] = lf


SecurityRequirementObject = Dict[str, List[str]]
# TODO: could do semantic test if requirements are defined in
# Components > SecuritySchemeObject


class DiscriminatorObject(BaseModel):
    propertyName: str
    mapping: Dict[str, str]


class XmlObject(BaseModel):
    name: Optional[str]
    namespace: Optional[str]
    prefix: Optional[str]
    attribute: bool = False
    wrapped: bool = False


class SchemaObject(BaseModel):
    type: str
    title: Optional[str]
    description: Optional[str]
    format: Optional[Union[Format | str]]
    enum: Optional[List[Any]]
    # const: Any
    externalDocs: Optional[ExternalDocumentationObject]
    example: Any
    deprecated: bool = False
    readOnly: bool = False
    writeOnly: bool = False
    nullable: bool = False  # Deprecated in version 3.1.0
    discriminator: Optional[DiscriminatorObject]
    xml: Optional[XmlObject]

    def __new__(cls, **kwargs):
        original_kwargs = kwargs
        if (reference := kwargs.get("$ref", None)) is not None:
            # It's too late if this happens in BaseModel.__init__
            kwargs = reference.get_dict()
        if any(op in kwargs for op in ["allOf", "anyOf", "oneOf", "not"]):
            kwargs["type"] = "combinator"
        subcls = {
            "integer": SchemaObjectInt,
            "number": SchemaObjectNumber,
            "string": SchemaObjectString,
            "boolean": SchemaObjectBoolean,
            "array": SchemaObjectArray,
            "object": SchemaObjectObject,
            "null": SchemaObjectNull,
            "combinator": SchemaCombinatoricObject,
            }.get(kwargs.get("type"), cls)
        return super().__new__(subcls, **original_kwargs)

    def render_example(self):
        return json.dumps(self.make_example(), indent=2)

    def make_example(self, minimal=False):
        if self.enum:
            return self.enum[0]
        if value := getattr(self, "default", None):
            return value
        if self.example:
            return self.example
        return self.make_typed_example(minimal)

    def make_typed_example(self, minimal):
        raise NotImplementedError(self.type)


class SchemaCombinatoricObject(SchemaObject):
    type: str = "combinator"
    allOf: Optional[List[SchemaObject]]
    oneOf: Optional[List[SchemaObject]]
    anyOf: Optional[List[SchemaObject]]
    not_: Optional[SchemaObject] = pydantic.Field(alias="not")
    combinator: str = ""
    merged: Optional[SchemaObject] = None

    def __init__(self, **values):
        super().__init__(**values)
        if (reference := values.get("$ref")) is not None:
            values = reference.get_dict()
        self.check_one_of(values, "allOf", "oneOf", "anyOf", "not")
        self.combinator = next(
            name for name in ["allOf", "anyOf", "oneOf", "not"]
            if name in values)
        if self.combinator == "allOf":
            self.merged = merge_schemas(values["allOf"])

    @pydantic.validator("allOf", "oneOf", "anyOf", allow_reuse=True)
    def combination_must_have_more_than_one_element(cls, v):
        if len(v) <= 1:
            raise ValueError("must have more than one element")
        return v

    @property
    def children(self):
        if self.combinator == "not":
            return [self.not_]
        return getattr(self, self.combinator)

    def make_typed_example(self, minimal):
        match self.combinator:
            case "allOf":
                return self.merged.make_example(minimal)
            case "anyOf" | "oneOf":
                return self.children[0].make_example(minimal)
            case "not":  # pragma: no branch
                schema = SchemaObject(type="string")
                if self.not_.type == "string":
                    schema = SchemaObject(type="integer")
                return schema.make_example(minimal)

    def dict(self, **kwargs):
        result = super().dict(**kwargs)
        result.pop("combinator", None)
        result.pop("merged", None)
        return result


class SchemaObjectInt(SchemaObject):
    default: Optional[int]
    multipleOf: Optional[pydantic.PositiveInt]
    maximum: Optional[int]
    exclusiveMaximum: Optional[int]
    minimum: Optional[int]
    exclusiveMinimum: Optional[int]

    def make_typed_example(self, minimal):
        for value in [self.minimum, self.maximum, self.multipleOf]:
            if value is not None:
                return value
        if self.exclusiveMinimum:
            return self.exclusiveMinimum + 1
        if self.exclusiveMaximum:
            return self.exclusiveMaximum - 1
        return 1


class SchemaObjectNumber(SchemaObject):
    default: Optional[float]
    multipleOf: Optional[pydantic.PositiveFloat]
    maximum: Optional[float]
    exclusiveMaximum: Optional[float]
    minimum: Optional[float]
    exclusiveMinimum: Optional[float]

    def make_typed_example(self, minimal):
        for value in [self.minimum, self.maximum, self.multipleOf]:
            if value is not None:
                return value
        if self.exclusiveMinimum:
            return self.exclusiveMinimum + 0.2
        if self.exclusiveMaximum:
            return self.exclusiveMaximum - 0.2
        return 3.141


class SchemaObjectString(SchemaObject):
    default: Optional[str]
    maxLength: Optional[pydantic.NonNegativeInt]
    minLength: Optional[pydantic.NonNegativeInt]
    pattern: Optional[str]

    def make_example(self, minimal=False):
        if self.format in [Format.string_date, Format.string_date_time] and \
                self.default is not None:
            value = DateTime.now()
            if (mm := timespec_re.match(self.default or "")) is not None:
                value -= TimeDelta(**{mm.group(2): int(mm.group(1))})
            elif self.default != "now":
                value = None
            if value is not None:
                if self.format == Format.string_date:
                    form = "%Y-%m-%d"
                else:
                    form = "%Y-%m-%dT%H:%M:%SZ"
                return value.strftime(form)
        return super().make_example(minimal)

    def make_typed_example(self, minimal):
        if self.pattern:
            return self.pattern
        match self.format:
            case Format.string_byte:
                return "\\x64"
            case Format.string_binary:
                return "".join(f"\\x{ord(v)}" for v in "Lorem ipsum")
            case Format.string_date:
                return "1969-07-20"
            case Format.string_date_time:
                return "1969-07-20T09:32:00 EDT"
            case Format.string_password:
                return "********"
            case Format.string_uuid:
                return "d9a4f688-feff-49ea-978c-3621daabcf47"
            case Format.string_regex:
                return r"^\s*# (.*)$"
            case "base64":
                return b64encode("Lorem ipsum")
        if self.minLength:
            return "a" * self.minLength
        return "value"


class SchemaObjectBoolean(SchemaObject):
    default: Optional[bool]

    def make_typed_example(self, minimal):
        return True


class SchemaObjectArray(SchemaObject):
    items: SchemaObject
    default: Optional[List[Any]]
    # these are called maxContains and minContains in JSONSchema:
    maxItems: Optional[pydantic.NonNegativeInt]
    minItems: pydantic.NonNegativeInt = 0
    uniqueItems: bool = False

    def make_typed_example(self, minimal):
        amount = self.minItems
        if amount == 0 and not minimal:
            amount = 1
        return [self.items.make_example(minimal) for x in range(amount)]


class SchemaObjectObject(SchemaObject):
    default: Optional[Dict[str, Any]]
    maxProperties: Optional[pydantic.NonNegativeInt]
    minProperties: pydantic.NonNegativeInt = 0
    properties: Dict[str, SchemaObject] = df
    required: List[str] = lf
    # dependentRequired: Dict[str, List[str]]
    additionalProperties: Union[bool, SchemaObject] = True

    def make_typed_example(self, minimal):
        rendition = {
            name: schema.make_example(minimal)
            for name, schema in self.properties.items()
            if name in self.required or not minimal}
        if self.additionalProperties and not minimal:
            if isinstance(self.additionalProperties, SchemaObject):
                value = self.additionalProperties.make_example(minimal)
            else:
                value = "value"
            rendition["additionalProperty"] = value
        return rendition


class SchemaObjectNull(SchemaObject):
    nullable: bool = True
    default: None = None

    def make_example(self, minimal=False):
        return None


class ParameterObject(BaseModel):
    name: str
    in_: ParameterLocation = pydantic.Field(alias="in")
    description: Optional[str]
    required: bool
    deprecated: bool = False
    allowEmptyValue: bool = False
    schema_: Optional[SchemaObject] = pydantic.Field(alias="schema")
    style: Style
    explode: bool
    allowReserved: bool = False
    content: Optional[Dict[str, "MediaTypeObject"]]
    examples: Dict[str, ExampleObject] = df
    example: Any

    @pydantic.validator("example", allow_reuse=True)
    def examples_mutually_exclusive(cls, v, values, **kwargs):
        if values["examples"]:
            raise ValueError("is mutually exclusive with `examples`")
        return v

    @pydantic.validator("content", allow_reuse=True)
    def can_have_only_one_content_mediatype(cls, v, values, **kwargs):
        if len(v) != 1:
            raise ValueError("can contain only one mediatype")
        return v

    @pydantic.root_validator(pre=True, allow_reuse=True)
    def must_have_content_or_schema(cls, values):
        return cls.check_one_of(values, "content", "schema")

    @pydantic.root_validator(pre=True, allow_reuse=True)
    def set_default_values(cls, values):
        values.setdefault("required", values.get("in") == "path")
        qc = values.get("in") in ["query", "cookie"]
        values.setdefault("style", "form" if qc else "simple")
        values.setdefault("explode", values.get("style") == "form")
        return values

    def dict(self, **kwargs):
        result = super().dict(**kwargs)
        defaults = self.set_default_values({"in": self.in_.value})
        if self.style.value != defaults["style"]:
            defaults = self.set_default_values(
                {"in": self.in_.value, "style": self.style.value})
            del defaults["style"]
        del defaults["in"]
        return {
            key: value for key, value in result.items()
            if value != defaults.get(key)}


class HeaderList(pydantic.BaseModel):
    __root__: Dict[str, ParameterObject]

    @pydantic.root_validator(pre=True)
    def set_parameter_config(cls, values):
        for key, data in values.get("__root__", {}).items():
            if not isinstance(data, dict):
                continue
            if "name" in data or "in" in data:
                raise ValueError("extra fields not permitted")
            data["name"] = key
            data["in"] = "header"
        return values

    def dict(self, **kwargs):
        result = super().dict(**kwargs)
        for value in result["__root__"].values():
            del value["name"]   # is always the key
            del value["in"]     # is always "header"
        return result


class EncodingObject(BaseModel):
    headers: HeaderList = df
    style: Optional[Style]
    contentType: Optional[str]  # gets default value if in requestBody
    explode: Optional[bool]     # gets default value if in requestBody
    allowReserved: bool = False


class MediaTypeObject(BaseModel):
    schema_: SchemaObject = pydantic.Field(alias="schema")
    examples: Dict[str, ExampleObject] = df
    example: Any
    encoding: Dict[str, EncodingObject] = df

    @pydantic.validator("example", allow_reuse=True)
    def examples_mutually_exclusive(cls, v, values, **kwargs):
        if values["examples"]:
            raise ValueError("is mutually exclusive with `examples`")
        return values

    @classmethod
    def check_encoding(cls, spec):
        if (encodings := spec.get("encoding")) is None:
            return
        elif spec.get("schema", {}).get("type") != "object":
            # I'm not sure about this
            raise ValueError("Encodings must refer to schema properties")
        schema_props = spec["schema"].get("properties", {})
        for propname, defs in encodings.items():
            if (prop := schema_props.get(propname)) is None:
                raise ValueError(
                    f"Encoding refers to unknown property {propname}")
            if "contentType" not in defs:
                if prop["type"] in ["object", "array"]:
                    defs["contentType"] = "application/json"
                elif prop["type"] == "string" and \
                        prop.get("format") == "binary":
                    defs["contentType"] = "application/octet-stream"
                else:
                    defs["contentType"] = "text/plain"
            defs.setdefault("explode", str(defs.get("style")) == "form")


class RequestBodyObject(BaseModel):
    content: Dict[str, MediaTypeObject]
    description: Optional[str]
    required: bool = False

    def __init__(self, **values):
        for mto_values in values.get("content", {}).values():
            MediaTypeObject.check_encoding(mto_values)
        super().__init__(**values)


SecuritySchemeObject = make_security_scheme_class(SecurityScheme)


class LinkObject(BaseModel):
    operationRef: Optional[str] = None
    operationId: Optional[str] = None
    parameters: Dict[str, Any] = df
    requestBody: Any
    description: Optional[str]
    server: Optional[ServerObject]

    @pydantic.root_validator
    def one_operation_reference_given(cls, values):
        return cls.check_one_of(values, "operationId", "operationRef")


class ResponseObject(BaseModel):
    description: str
    headers: HeaderList = df
    content: Dict[str, MediaTypeObject] = df
    links: Dict[str, LinkObject] = df


class OperationObject(BaseModel):
    responses: Dict[int, ResponseObject]
    tags: List[str] = lf
    summary: Optional[str]
    description: Optional[str]
    externalDocs: Optional[ExternalDocumentationObject]
    operationId: Optional[str]
    parameters: List[ParameterObject] = lf
    requestBody: Optional[RequestBodyObject]
    callbacks: Dict[str, Dict[str, "PathItemObject"]] = df
    security: List[SecurityRequirementObject] = df
    servers: List[ServerObject] = lf
    deprecated: bool = False

    oauth_scopes = get_oauth_scopes


class PathItemObject(BaseModel):
    OPNAMES = [
        "get", "put", "post", "delete",
        "options", "head", "patch", "trace"]
    summary: Optional[str]
    description: Optional[str]
    get: Optional[OperationObject]
    put: Optional[OperationObject]
    post: Optional[OperationObject]
    delete: Optional[OperationObject]
    options: Optional[OperationObject]
    head: Optional[OperationObject]
    patch: Optional[OperationObject]
    trace: Optional[OperationObject]
    servers: List[ServerObject] = lf
    parameters: List[ParameterObject] = lf

    class Config:
        fields = {'OPNAMES': {'exclude': True}}

    @property
    def operations(self):
        return {
            name: value
            for name, value in self.__dict__.items()
            if name in self.OPNAMES and value is not None}


class ComponentsObject(BaseModel):
    schemas: Dict[str, SchemaObject] = df
    responses: Dict[str, ResponseObject] = df
    parameters: Dict[str, ParameterObject] = df
    examples: Dict[str, ExampleObject] = df
    requestBodies: Dict[str, RequestBodyObject] = df
    headers: HeaderList = df
    securitySchemes: Dict[str, SecuritySchemeObject] = df  # type: ignore
    links: Dict[str, LinkObject] = df
    callbacks: Dict[str, PathItemObject] = df


class OpenApiV3Spec(BaseModel, ApiSpec):
    type: str = "openapi"
    info: InfoObject
    paths: Dict[str, PathItemObject]
    servers: List[ServerObject] = lf
    components: ComponentsObject = cf(ComponentsObject)
    security: SecurityRequirementObject = df
    tags: List[TagObject] = lf
    externalDocs: Optional[ExternalDocumentationObject]

    @property
    def endpoints(self):
        return self.paths


OperationObject.update_forward_refs()
ParameterObject.update_forward_refs()
