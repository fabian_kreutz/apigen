from enum import Enum
from markdown import markdown
from typing import Dict, Optional, Any
import pydantic
from rfc3986_validator import validate_rfc3986  # type: ignore


lf = pydantic.Field(default_factory=list)
df = pydantic.Field(default_factory=dict)


def cf(class_):
    return pydantic.Field(default_factory=class_)


def empty(value):
    if isinstance(value, (dict, list, str)):
        return not bool(value)
    # A value of boolean/false or int/0 is not empty
    return value is None


class BaseModel(pydantic.BaseModel):
    extensions: Dict[str, Any] = cf(dict)
    referenceName: Optional[str] = None

    class Config:
        extra = "forbid"

    def __init__(self, **values):
        if (reference := values.pop("$ref", None)) is not None:
            if values:
                raise ValueError("No attributes next to $ref allowed")
            if reference.is_initialized():
                return
            values = reference.get_dict()
            reference.mark_initialized()
        ext = {}
        for key in list(values.keys()):
            if key.startswith("x-"):
                ext[key[2:]] = values.pop(key)
        values.setdefault("extensions", {}).update(ext)
        super().__init__(**values)

    def __new__(cls, **values):
        # The catalog feature is further explained in the project documentation
        if (reference := values.pop("$ref", None)) is not None:
            if (obj := reference.get_object()) is not None:
                return obj
        obj = super().__new__(cls)
        if reference:
            reference.set_object(obj)
        return obj

    @classmethod
    def check_one_of(cls, values, *fields):
        num_given_fields = sum(int(bool(values.get(ff))) for ff in fields)
        if num_given_fields != 1:
            raise ValueError(
                f"Exactly one of {', '.join(fields)} must be given")
        return values

    @property
    def description_md(self):
        if not hasattr(self, "description"):
            raise AttributeError("Model has no description")
        return markdown(self.description)

    def dict(self, **kwargs):
        kwargs["by_alias"] = True
        if self.referenceName:
            return {"$ref": f"#/components/{self.referenceName}"}
        schema = self.schema(by_alias=True)
        if "$ref" in schema:
            name = schema["$ref"].split("/")[-1]
            schema = schema["definitions"][name]
        properties = schema.get("properties", {})
        result = {
            key: val.value if isinstance(val, Enum) else val
            for key, val in super().dict(**kwargs).items()
            if not empty(val) and
            val != properties.get(key, {}).get("default")}
        for key, value in result.pop("extensions", {}).items():
            result[f"x-{key}"] = value
        return result


def rfc3986_validator(value: Optional[str]) -> Optional[str]:
    if value is not None and not validate_rfc3986(value):
        raise ValueError(f"Not a valid url: {value}")
    return value


def url_validator(name="url"):
    return pydantic.validator(name, allow_reuse=True)(rfc3986_validator)


def json_merge(base, patch):
    if isinstance(patch, dict):
        if not isinstance(base, dict):
            base = {}
        for key, value in patch.items():
            if value is None:
                if key in base:
                    del base[key]
            else:
                base[key] = json_merge(base.get(key), value)
        return base
    return patch
