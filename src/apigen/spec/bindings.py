from enum import Enum
from typing import Optional
from pydantic import Field
from .definitions import BaseModel
from .openapi import SchemaObjectObject, SchemaObject


# DOC_BASE = https://github.com/asyncapi/bindings/blob/master


# Http: DOC_BASE/http
class HttpOperationBindingObject(BaseModel):
    class HttpDirection(Enum):
        request = "request"
        response = "response"

    class HttpMethod(Enum):
        get = "get"
        post = "post"
        put = "put"
        patch = "patch"
        delete = "delete"
        head = "head"
        options = "options"
        connect = "connect"
        trace = "trace"

    type: HttpDirection
    method: HttpMethod | None
    query: Optional[SchemaObjectObject]
    bindingVersion: str = "0.1.0"


class HttpMessageBindingObject(BaseModel):
    headers: SchemaObjectObject
    bindingVersion: str = "0.1.0"


# WebSockets: DOC_BASE/websockets
class WsChannelBindingObject(BaseModel):
    class WsMethod(Enum):
        get = "get"
        post = "post"

    method: WsMethod
    query: Optional[SchemaObjectObject]
    headers: Optional[SchemaObjectObject]
    bindingVersion: str = "0.1.0"


# Kafka: DOC_BASE/kafka/README.md
class KafkaServerBindingObject(BaseModel):
    schemaRegistryUrl: Optional[str]
    schemaRegistryVendor: Optional[str]
    bindingVersion: str = "0.4.0"


class KafkaChannelBindingObject(BaseModel):
    class TopicConfiguration(BaseModel):
        cleanup_policy: list[str] = \
            Field(default_factory=list, alias="cleanup.policy")
        retention_ms: Optional[int] = Field(alias="retention.ms")
        retention_bytes: Optional[int] = Field(alias="retention.bytes")
        delete_retention_ms: Optional[int] = \
            Field(alias="delete.retention.ms")
        max_message_bytes: Optional[int] = Field(alias="max.message.bytes")

    topic: Optional[str]
    partitions: Optional[int] = Field(gt=0)
    replicas: Optional[int] = Field(gt=0)
    topicConfiguration: Optional[TopicConfiguration]
    bindingVersion: str = "0.4.0"


class KafkaOperationBindingObject(BaseModel):
    groupId: Optional[SchemaObjectObject]
    clientId: Optional[SchemaObjectObject]
    bindingVersion: str = "0.4.0"


class KafkaMessageBindingObject(BaseModel):
    key: Optional[SchemaObjectObject]
    schemaIdLocation: Optional[str]
    schemaIdPayloadEncoding: Optional[str]
    schemaLookupStrategy: Optional[str]
    bindingVersion: str = "0.4.0"


# Active MQ: DOC_BASE/ampq/README.md
class AmqpChannelBindingObject(BaseModel):
    class AmqpChannelBindingExchange(BaseModel):
        class ExchangeType(Enum):
            topic = "topic"
            direct = "direct"
            fanout = "fanout"
            default = "default"
            headers = "headers"

        name: str = Field(max_length=255)
        type: ExchangeType
        durable: bool
        autoDelete: bool
        vhost: str = "/"

    class AmqpChannelBindingQueue(BaseModel):
        name: str = Field(max_length=255)
        durable: bool
        exclusive: bool
        autoDelete: bool
        vhost: str = "/"

    is_: str = Field(default="routingKey", alias="is")
    exchange: Optional[AmqpChannelBindingExchange]
    queue: Optional[AmqpChannelBindingQueue]
    bindingVersion: str = "0.2.0"


class AmqpOperationBindingObject(BaseModel):
    expiration: Optional[int] = Field(ge=0)
    userId: Optional[str]
    cc: list[str] = Field(default_factory=list)
    priority: Optional[int]
    deliveryMode: Optional[int] = Field(ge=1, le=2)
    mandatory: Optional[bool]
    bcc: list[str] = Field(default_factory=list)
    replyTo: Optional[str]
    timestamp: Optional[bool]
    ack: Optional[bool]
    bindingVersion: str = "0.2.0"


class AmqpMessageBindingObject(BaseModel):
    contentEncoding: Optional[str]
    messageType: Optional[str]
    bindingVersion: str = "0.2.0"


# MQTT: DOC_BASE/mqtt/README.md
class MqttServerBindingObject(BaseModel):
    class LastWill(BaseModel):
        topic: Optional[str]
        qos: Optional[int] = Field(ge=0, le=2)
        message: Optional[str]
        retain: Optional[bool]

    clientId: Optional[str]
    cleanSession: Optional[bool]
    lastWill: Optional[LastWill]
    keepAlive: Optional[int]
    bindingVersion: str = "0.1.0"


class MqttOperationBindingObject(BaseModel):
    qos: Optional[int] = Field(ge=0, le=2)
    retain: Optional[bool]
    bindingVersion: str = "0.1.0"


class Mqtt5ServerBindingObject(BaseModel):
    sessionExpiryInterval: SchemaObject | int
    bindingVersion: str = "0.2.0"


# Nats: DOC_BASE/nats
class NatsOperationBindingObject(BaseModel):
    query: str = Field(max_length=255)
    bindingVersion: str = "0.1.0"


# Java Messaging Service: DOC_BASE/jms
class JmsServerBindingObject(BaseModel):
    jmsConnectionFactory: str
    properties: list[SchemaObject] = Field(default_factory=list)
    clientId: Optional[str]
    bindingVersion: str = "0.0.1"


class JmsChannelBindingObject(BaseModel):
    class DestinationType(Enum):
        queue = "queue"
        fifo_queue = "fifo-queue"

    destination: Optional[str]
    destinationType: DestinationType = DestinationType.queue
    bindingVersion: str = "0.0.1"


class JmsMessageBindingObject(BaseModel):
    headers: SchemaObjectObject
    bindingVersion: str = "0.0.1"


# AnypointMQ: DOC_BASE/anypointmq
AnypointMqChannelBindingObject = JmsChannelBindingObject
AnypointMqMessageBindingObject = JmsMessageBindingObject


# Solace: DOC_BASE/solace
class SolaceServerBindingObject(BaseModel):
    msgVpn: str
    bindingVersion: str = "0.3.0"


class SolaceOperationBindingObject(BaseModel):
    class Destination(BaseModel):
        class DestinationType(Enum):
            queue = "queue"
            topic = "topic"

        class DeliveryMode(Enum):
            direct = "direct"
            persistent = "persistent"

        class Queue(BaseModel):
            class AccessType(Enum):
                exclusive = "exclusive"
                nonexclusive = "nonexclusive"

            name: Optional[str]
            topicSubscriptions: Optional[list[str]]
            accessType: Optional[AccessType]
            maxMsgSpoolSize: Optional[str]
            maxTtl: Optional[str]

        class Topic(BaseModel):
            topicSubscriptions: Optional[list[str]]

        destinationType: DestinationType
        deliveryMode: DeliveryMode = DeliveryMode.persistent
        queue: Optional[Queue]
        topic: Optional[Topic]

    destination: list[Destination] = Field(default_factory=list)
    bindingVersion: str = "0.3.0"


# Google Cloud Pub/Sub: DOC_BASE/googlepubsub
class GooglePubSubChannelBindingObject(BaseModel):
    class MessageStoragePolicy(BaseModel):
        allowedPersistenceRegions: list[str]

    class SchemaSettings(BaseModel):
        class Encoding(Enum):
            encoding_unspecified = "ENCODING_UNSPECIFIED"
            json = "JSON"
            binary = "BINARY"

        encoding: Encoding
        firstRevisionId: str
        lastRevisionId: str
        name: str

    labels: dict[str, str] = Field(default_factory=dict)
    messageRetentionDuration: Optional[str]
    messageStoragePolicy: MessageStoragePolicy
    schemaSettings: Optional[SchemaSettings]
    topic: Optional[str]
    bindingVersion: str = "0.1.0"


class GooglePubSubMessageBindingObject(BaseModel):
    class SchemaDefinition(BaseModel):
        name: str
        type: str

    attributes: dict[str, str] = Field(default_factory=dict)
    orderingKey: Optional[str]
    schema_: Optional[SchemaDefinition] = Field(alias="schema")
    bindingVersion: str = "0.1.0"


# Pulsar: DOC_BASE/pulsar
class PulsarServerBindingObject(BaseModel):
    tenant: Optional[str]
    bindingVersion: str = "0.1.0"


class PulsarChannelBindingObject(BaseModel):
    class RetentionDefinition(BaseModel):
        time: int = 0
        size: int = 0

    namespace: str
    persistence: str
    compaction: Optional[int]
    geo_replication: Optional[list[str]] = Field(alias="geo-replication")
    retention: Optional[RetentionDefinition]
    ttl: Optional[int]
    deduplication: Optional[bool]
    bindingVersion: str = "0.1.0"


# ---------------------------------------------------


class ServerBindingsObject(BaseModel):
    # http
    # ws
    kafka: Optional[KafkaServerBindingObject]
    # anypointmq
    # amqp
    # amqp1
    mqtt: Optional[MqttServerBindingObject]
    mqtt5: Optional[Mqtt5ServerBindingObject]
    # nats
    jms: Optional[JmsServerBindingObject]
    # sns
    solace: Optional[SolaceServerBindingObject]
    # sqs
    # stomp
    # redis
    # mercure
    # googlepubsub
    pulsar: Optional[PulsarServerBindingObject]


class ChannelBindingsObject(BaseModel):
    # http
    ws: Optional[WsChannelBindingObject]
    kafka: Optional[KafkaChannelBindingObject]
    anypointmq: Optional[AnypointMqChannelBindingObject]
    amqp: Optional[AmqpChannelBindingObject]
    # amqp1
    # mqtt
    # mqtt5
    # nats
    jms: Optional[JmsChannelBindingObject]
    # sns
    # solace
    # sqs
    # stomp
    # redis
    # mercure
    googlepubsub: Optional[GooglePubSubChannelBindingObject]
    pulsar: Optional[PulsarChannelBindingObject]

    def __iter__(self):
        return iter(name for name in self.__fields__
                    if getattr(self, name))


class MessageBindingsObject(BaseModel):
    http: Optional[HttpMessageBindingObject]
    # ws
    kafka: Optional[KafkaMessageBindingObject]
    anypointmq: Optional[AnypointMqMessageBindingObject]
    amqp: Optional[AmqpMessageBindingObject]
    # amqp1
    # mqtt
    # mqtt5
    # nats
    jms: Optional[JmsMessageBindingObject]
    # sns
    # solace
    # sqs
    # stomp
    # redis
    # mercure
    googlepubsub: Optional[GooglePubSubMessageBindingObject]
    # pulsar


class OperationBindingsObject(BaseModel):
    http: Optional[HttpOperationBindingObject]
    # ws
    kafka: Optional[KafkaOperationBindingObject]
    # anypointmq
    amqp: Optional[AmqpOperationBindingObject]
    # amqp1
    mqtt: Optional[MqttOperationBindingObject]
    # mqtt5
    nats: Optional[NatsOperationBindingObject]
    # jms
    # sns
    solace: Optional[SolaceOperationBindingObject]
    # sqs
    # stomp
    # redis
    # mercure
    # googlepubsub
    # pulsar
