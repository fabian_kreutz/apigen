from itertools import count
from slugify import slugify
from .spec.openapi import SchemaObject


def unique_in(values, name):
    if name not in values:
        return name
    for ii in count(2):  # pragma: no cover
        if f"{name}{ii}" not in values:
            return f"{name}{ii}"


def extract_components(spec):
    if spec.type != "openapi":
        raise NotImplementedError(
            "Extracting AsyncAPI components not supported yet")
    schemaComponents = spec.components.schemas
    for view in spec.paths.values():
        for op, data in view.operations.items():
            opId = f"{op.capitalize()}Response"
            if data.operationId:
                opId = slugify(data.operationId).capitalize() + "Response"
            for code, resp in data.responses.items():
                for content in resp.content.values():
                    schema = content.schema_
                    if schema.referenceName:
                        continue
                    sdata = schema.dict()
                    sdata.pop("title", None)
                    for schemaName, externalSchema in schemaComponents.items():
                        if sdata == externalSchema.dict():
                            schema.referenceName = "schemas/" + schemaName
                            break
                    else:
                        schemaName = unique_in(
                            schemaComponents.keys(),
                            schema.title or opId)
                        copy = SchemaObject(**sdata)
                        schemaComponents[schemaName] = copy
                        schema.referenceName = "schemas/" + schemaName
