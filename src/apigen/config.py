import argparse
import configparser
from pathlib import Path
from typing import Any
from . import logging
from .logging import verbosity_to_loglevel
from .exceptions import ApigenError
from .version import version


Config = dict[str, Any]


class Subcommand:
    file_config: configparser.ConfigParser

    @classmethod
    def make_argparser(cls, subcommands):
        parser = argparse.ArgumentParser(prog="apigen")
        parser.add_argument('-V', '--version',
                            action='version', version=version)
        loglevel_choices = verbosity_to_loglevel + \
            [name[0] for name in verbosity_to_loglevel] + \
            [str(ii) for ii in range(len(verbosity_to_loglevel))]
        parser.add_argument(
            '-v', '--verbosity', default=None, choices=loglevel_choices,
            help=f"Set verbosity as {', '.join(verbosity_to_loglevel)}")
        parser.add_argument('-c', '--config-file',
                            type=Path,
                            help="Path to the configuration file")
        subparsers = parser.add_subparsers(title="subcommands")
        for sub_class in subcommands:
            sp = subparsers.add_parser(
                sub_class.__name__.lower(), help=sub_class.__doc__)
            sp.set_defaults(command=sub_class)
            sub_class.add_arguments(sp)
        return parser

    @classmethod
    def add_arguments(cls, parser):
        pass

    def __init__(self, args: argparse.Namespace):
        self.file_config = fc = self.read_config_file(args.config_file)
        logging.configure(
            args.verbosity,
            loglevel=fc.get("logging", "loglevel", fallback="INFO"),
            logfile=fc.get("logging", "logfile", fallback="+"))

    def read_config_file(self, path: Path) -> configparser.ConfigParser:
        cp = configparser.ConfigParser()
        if path is None and not (path := Path(f"{__package__}.ini")).exists():
            return cp
        if not path.exists():
            raise ApigenError(f"File not found: `{path}`")
        try:
            cp.read(str(path))
        except configparser.Error as ce:
            raise ApigenError(str(ce))
        return cp


def parse_args(argv, subcommands) -> Subcommand:
    argparser = Subcommand.make_argparser(subcommands)
    args = argparser.parse_args(argv)
    return args.command(args)
