from datetime import date as Date
from itertools import count
import jinja2
from pathlib import Path
from slugify import slugify
from . import BaseGenerator
from ..version import version


class HtmlGenerator(BaseGenerator):
    target_suffix = "html"

    def __init__(self, **kwargs):
        style = kwargs.get("style", "basic")
        loaders = [
            jinja2.PackageLoader(__package__, f"templates/html/{style}"),
            jinja2.PackageLoader(__package__, "templates/html/common")]
        if (template_dir := kwargs.get("template_dir")) is not None:
            loaders.insert(0, jinja2.FileSystemLoader(Path(template_dir)))
        self.env = jinja2.Environment(
            loader=jinja2.ChoiceLoader(loaders))
        self.env.filters["slugify"] = slugify
        self.env.globals["make_id"] = count().__next__
        self.render_context = kwargs.get("render_context", {})
        self.render_context.update({
            key[3:]: value
            for key, value in kwargs.items()
            if key.startswith("rc.")})
        self.render_context.setdefault("base_url", "/")
        self.render_context.setdefault("theme", "basic")

    def extra_context(self) -> dict[str, str]:
        result = {
            "generator_version": version,
            "date_created": str(Date.today())}
        result.update(self.render_context)
        return result

    def render(self, spec):
        template = self.env.get_template("index.html.jinja")
        return template.render(spec=spec, **self.extra_context())
