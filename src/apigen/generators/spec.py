import json
import yaml
from . import BaseGenerator


class JsonGenerator(BaseGenerator):
    target_suffix = "json"

    def render(self, spec):
        return json.dumps(spec.dict(exclude_none=True))


class YamlGenerator(BaseGenerator):
    target_suffix = "yaml"

    def render(self, spec):
        return yaml.dump(spec.dict(exclude_none=True))
