import abc
from ..spec import ApiSpec


class BaseGenerator(metaclass=abc.ABCMeta):
    target_suffix: str

    @abc.abstractmethod
    def render(self, spec: ApiSpec) -> str:
        raise NotImplementedError()
