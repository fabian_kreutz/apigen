from apigen.spec import openapi


class SpecDiffer:
    validate_references = False
    validate_array_order = False
    validate_ignore = None

    def __init__(self, **options):
        self.validate_ignore = []
        for key, value in options.items():
            if key.startswith("validate") and hasattr(self, key):
                setattr(self, key, value)
            else:
                raise ValueError(f"Unknown option `{key}`")

    def diff(self, spec1, spec2):
        spec1 = spec1.deepcopy()
        spec2 = spec2.deepcopy()
        if not self.validate_references:
            spec1.components = openapi.ComponentsObject()
            spec2.components = openapi.ComponentsObject()
            self.remove_references(spec1)
            self.remove_references(spec2)
        return list(self.iter_differences("/", spec1.dict(), spec2.dict()))

    def remove_references(self, data):
        data.referenceName = None
        for key, field in data.__fields__.items():
            value = getattr(data, key)
            if value is None:
                continue
            if isinstance(value, openapi.BaseModel):
                self.remove_references(value)
            elif isinstance(value, list) and \
                    isinstance(field.type_, type) and \
                    issubclass(field.type_, openapi.BaseModel):
                for obj in value:
                    self.remove_references(obj)
            elif isinstance(value, dict) and \
                    isinstance(field.type_, type) and \
                    issubclass(field.type_, openapi.BaseModel):
                for obj in value.values():
                    self.remove_references(obj)

    def iter_differences(self, path, data1, data2):
        for key in data1.keys():
            if f"{path}{key}" in self.validate_ignore:
                continue
            if key not in data2:
                yield f"Only in left:  {path}{key}"
                continue
            v1, v2 = data1[key], data2[key]
            if not self.validate_array_order:
                if key == "parameters":
                    v1 = {item["name"]: item for item in v1}
                    v2 = {item["name"]: item for item in v2}
                if key in ["oneOf", "anyOf", "allOf"]:
                    v2, old = [], v2
                    for elem in v1:
                        if elem in old:
                            old.remove(elem)
                        else:
                            elem = None
                        v2.append(elem)
                    v2.extend(old)
            if v1 != v2:
                if isinstance(v1, dict) and isinstance(v2, dict):
                    yield from self.iter_differences(f"{path}{key}/", v1, v2)
                else:
                    yield f"Difference in  {path}{key}: {v1} != {v2}"
        for key in data2.keys():
            if f"{path}{key}" in self.validate_ignore:
                continue
            if key not in data1:
                yield f"Only in right: {path}{key}"
