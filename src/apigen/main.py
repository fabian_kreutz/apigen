import sys
from . import logging
from .config import parse_args
from .exceptions import ApigenError
from .generate_command import Generate
from .validate_command import Validate


def main(argv=sys.argv[1:]):
    try:
        cmd = parse_args(argv, [Generate, Validate])
        cmd.run()
    except ApigenError as age:
        logging.logger.error(str(age))
        sys.exit(1)
