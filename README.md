# OpenAPI and AsyncAPI parser and generator

This project is supposed to parse api specs and call pluggable generators
to produce e.g. documentation.

## Documentation

See [main documentation](documentation/index.md).

## Testing or standalone usage

Use [tox](https://tox.wiki/en/latest/):

```sh
$ pip install tox
$ tox
$ .tox/py3/bin/apigen -h
$ .tox/py3/bin/apigen generate spec.json -C rc.base_url=/ -o api.html
```

## TODO

So far this project fills my own need and so does not support as much as it could.

* when reading or writing to stdin/stdout during generation, /dev/std\* is used, which probably does not work on all platforms.
* templates can use additional rendering context, but how can a template directory specify default values for it?

### Unsupported

* The HTML generator is pretty basic and does not deal with all data yet.
* Parser can currently support only a single (latest) api version
  * Even though changes are rare and even so called "breaking changes" hardly affect the structure, a parser should handle versions better
  * https://github.com/OAI/OpenAPI-Specification/releases
