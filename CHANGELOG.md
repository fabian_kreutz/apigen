# Changelog

## Version 0

* 0.5
  * Combinatoric schema improvements
    * allOf in HTML renders merged schema
    * fix to actually merge schemas which are references
  * Added utility to extract responses into components
  * Combinatoric schema improvements
    * Added missing coverage test and fixed typo in ValueError
    * Empty schemas assume that type is missing (and not combinator)
  * Included combinatoric schema arrays in "validate_array_order" exception
  * The internal "combinator" attribute removed from Schema.dict()
  * Added some empty defaults to Models for easier use
  * Generated example can be minimal
  * Support validation by comparing two specs
  * Altered CLI interface: use subcommands generate/validate
    * Deprecate the `[base]` configuration section
  * MediaContentType.Schema is actually required and not optional
  * Main ApiSpec object publishes its acronym by looking at the title
  * Don't remove empty values, don't crash on lists of native values
  * Show endpoint navbar if no tags defined
  * Show asyncapi servers from dict (list in openapi)
  * Small fix concerning security scheme serialization
  * Support ignoring a branch while validating
  * AsyncAPI bindings implemented according to current spec
  * Long anticipated better logging configuration
  * Support stderr for logging and stdin/stdout for generation
  * Support python 3.11 and going forward to pydantic 2
    * pydantic 2 is not yet supported due to lack of documentation,
      backwards-compatibility and bugs in it.
  * Fix validation error when combinatoric schema is a reference
  * Remove unnecessary constructor call.  The caller of `__new__` calls it
  * Response reference names need not be HTTP response code
  * Fix verbosity help text
  * Support deprecated "nullable" schema attribute
  * Do not choke while parsing recursive schema
    * Known bug: rendering recursive schema still fails
  * Fix bug: Allow traits to be references
* 0.4
  * Added json and yaml generators
    * Now for each programming language we can implement a parser
      that reads the code and produces as spec; then this is dumped as file
    * BaseModel.referenceName now also contains the component name
  * Made parsing as dynamical as generation
    * Spec parsing is now only one (the only included) implementation
  * URL validator does not crash on explicitly given None value
* 0.3
  * Fix path to templates for correct package installation
  * Remove too strict url validator
    * orchitect later translates non-rfc conformant urls like doc:fts
  * Allow AsyncAPI version 2.3, as it has only minor differences to 2.4
  * Implement channel and object schema rendering
  * Implement component schema rendering and add components to navbar
  * Rendering of multiple example options in a table
  * Allow cli to provide render context via "-C rc.key=value"
  * Added documentation and py.typed files and extra (superfluous?) type annotations
  * Import cycle solved by lazy loading generator endpoints
  * Remove branding from basic style
  * Responses keys (http status codes) changed to integer type
  * Add py.typed files to MANIFEST to have them included in packages
* 0.2
  * Implement HTML renderer for OpenApiV3 with jinja
  * Implement large part of HTML templates
  * Support markdown for descriptions
  * Model AsyncApi 2.4 specification
  * Adjust HTML rendition to cope with AsyncApi
  * Rearrange templates into reusable hierarchy
  * Implement schema and examples rendering
* 0.1
  * Implement CLI: parse arguments and config file
  * Initialize gitlab ci: tests, package and badges
  * Support pluggable generators from pkg_resource entry_points
  * Model OpenAPI v3 specification
