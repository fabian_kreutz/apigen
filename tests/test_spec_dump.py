import json
from pathlib import Path
import pytest
import yaml
from apigen.generators.spec import JsonGenerator
from apigen.generators.spec import YamlGenerator
from apigen.parsers.spec import SpecParser
from helpers.factory import minimal_struct


@pytest.mark.parametrize(
    "path", [str(p) for p in Path("tests", "regression").glob("*.json")])
def test_json_generator(path):
    path = Path(path)
    spec = SpecParser().parse(path)
    result = JsonGenerator().render(spec)
    assert json.loads(result) == json.load(path.open())


@pytest.mark.parametrize(
    "path", [str(p) for p in Path("tests", "regression").glob("*.json")])
def test_yaml_generator(path):
    path = Path(path)
    spec = SpecParser().parse(path)
    result = yaml.safe_load(YamlGenerator().render(spec))
    if "paths" in result:
        # The integer key of responses (http status code) is rendered
        # as string by json, which does not allow integer keys
        for pathobj in result["paths"].values():
            for op in pathobj.values():
                if "responses" in op:
                    op["responses"] = {
                        str(status): resp
                        for status, resp in op["responses"].items()}
    assert result == json.load(path.open())


def test_renders_references():
    struct = {
        "openapi": "3.0.1",
        "info": {"title": "Test API", "version": "1.2.3"},
        "paths": {
            "/go": {
                "get": {
                    "parameters": [
                        {"name": "id", "in": "header",
                         "schema": {"$ref": "#/components/schemas/IdParam"}},
                    ],
                    "responses": {"200": {"description": "Success"}}}}},
        "components": {
            "schemas": {
                "IdParam": {"type": "str"}}}}
    spec = SpecParser().parse_structure(struct.copy())
    assert json.loads(JsonGenerator().render(spec)) == struct


def test_renders_extension_parameters():
    struct = minimal_struct()
    struct["paths"]["/go"]["get"]["x-distraction"] = "Something shiny"
    spec = SpecParser().parse_structure(struct.copy())
    assert json.loads(JsonGenerator().render(spec)) == struct


def test_special_case_with_non_default_schema_style():
    struct = minimal_struct()
    get = struct["paths"]["/go"]["get"]
    # in=header means that style is by default "simple"
    # if style where "simple", then explode=False is default
    # now that style is explicitly "form", explode=False is not default
    get["parameters"] = [{
        "name": "id", "in": "header",
        "style": "form", "explode": False,
        "schema": {"type": "string"}}]
    spec = SpecParser().parse_structure(struct.copy())
    assert json.loads(JsonGenerator().render(spec)) == struct


def test_no_internal_attributes_in_combinatoric_schema():
    struct = minimal_struct()
    get = struct["paths"]["/go"]["get"]
    get["requestBody"] = {"content": {"application/json": {"schema": {
        "anyOf": [
            {"type": "number"},
            {"type": "integer"},
            {"not": {"type": "string"}},
        ]}}}}
    spec = SpecParser().parse_structure(struct.copy())
    assert json.loads(JsonGenerator().render(spec)) == struct
