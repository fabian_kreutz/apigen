import pytest
import os
from helpers.logging import LogBucket


@pytest.fixture
def lg(caplog):
    return LogBucket(caplog)


@pytest.fixture
def in_tmpdir(tmp_path):
    current_dir = os.getcwd()
    os.chdir(tmp_path)
    yield
    os.chdir(current_dir)
