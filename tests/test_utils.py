import pytest
from apigen.spec.definitions import json_merge
from apigen.spec.openapi import OpenApiV3Spec
from apigen.utils import extract_components


def make_spec(**paths):
    return OpenApiV3Spec(**{
        "api_version": "3.0.1",
        "info": {"title": "Test API", "version": "1.2.3"},
        "paths": paths})


def test_json_merge():
    base = {
        "title": "Goodbye!",
        "author": {
            "givenName": "John",
            "familyName": "Doe"
        },
        "tags": ["example", "sample"],
        "content": "This will be unchanged"}
    patch = {
        "title": "Hello!",
        "phoneNumber": "+01-123-456-7890",
        "author": {
            "familyName": None,
            "notThereAnyways": None},
        "tags": ["example"]}
    result = json_merge(base, patch)
    assert result == {
        "title": "Hello!",
        "author": {
            "givenName": "John"},
        "tags": ["example"],
        "content": "This will be unchanged",
        "phoneNumber": "+01-123-456-7890"}


@pytest.mark.parametrize("idempotent", [True, False])
def test_success_responses_are_extracted_as_components(idempotent):
    spec = make_spec(**{"go/": {"get": {"responses": {200: {
        "description": "Success",
        "content": {"application/json": {"schema": {
            "type": "object",
            "properties": {"name": {"type": "string"}},
        }}}}}}}})
    extract_components(spec)
    if idempotent:
        extract_components(spec)
    schema = spec.paths["go/"].get\
        .responses[200].content["application/json"].schema_
    assert schema.referenceName == "schemas/GetResponse"
    assert spec.components.schemas["GetResponse"]


def test_same_error_class_of_two_operations_same_component():
    error_content = {"application/json": {"schema": {
        "type": "object",
        "properties": {
            "result": {"type": "string", "default": "error"},
            "error": {"type": "string"}}}}}
    error_responses = {"get": {"responses": {
        401: {
            "description": "Authentication failed",
            "content": error_content},
        403: {
            "description": "Access denied",
            "content": error_content}}}}
    spec = make_spec(**{
        "come/": error_responses,
        "go/": error_responses})
    extract_components(spec)
    for path in spec.paths.values():
        for resp in path.get.responses.values():
            assert resp.content["application/json"].schema_.referenceName \
                == "schemas/GetResponse"
    assert sorted(spec.components.schemas["GetResponse"].properties.keys()) \
        == ["error", "result"]


def test_does_not_overwrite_existing_components():
    component = {
        "type": "object",
        "properties": {"name": {"type": "string"}, "age": {"type": "integer"}}}
    spec = OpenApiV3Spec(**{
        "api_version": "3.0.1",
        "info": {"title": "Test API", "version": "1.2.3"},
        "paths": {"go/": {"get": {"responses": {200: {
            "description": "Success",
            "content": {"application/json": {"schema": {
                **component, "additionalProperties": False}}}}}}}},
        "components": {"schemas": {
            "GetResponse": component,
            "GetResponse2": component}},
        })
    extract_components(spec)
    schemas = spec.components.schemas
    resp = spec.paths["go/"].get.responses[200].content["application/json"]
    assert schemas["GetResponse"].additionalProperties is True
    assert schemas["GetResponse2"].additionalProperties is True
    assert resp.schema_.referenceName == "schemas/GetResponse3"
    assert schemas["GetResponse3"].additionalProperties is False


def test_response_object_typename_is_operation_id():
    spec = make_spec(**{"go/": {"get": {
        "operationId": "goOutAndGetHerJude",
        "responses": {200: {
            "description": "OK",
            "content": {"application/json": {"schema": {
                "type": "string"}}}
        }}}}})
    extract_components(spec)
    assert "GooutandgetherjudeResponse" in spec.components.schemas
    resp = spec.paths["go/"].get.responses[200].content["application/json"]
    assert resp.schema_.referenceName == "schemas/GooutandgetherjudeResponse"
