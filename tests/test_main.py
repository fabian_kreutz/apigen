import logging
import os
import pytest
import sys
from apigen import main, version
from apigen.config import parse_args, Subcommand
from apigen.logging import logger
from helpers.main import MainHelper


@pytest.fixture
def mh(tmp_path):
    return MainHelper(tmp_path)


@pytest.fixture
def in_tmpdir(tmp_path):
    current_dir = os.getcwd()
    os.chdir(tmp_path)
    yield
    os.chdir(current_dir)


def test_shows_help(capsys):
    with pytest.raises(SystemExit):
        main.main(["--help"])
    out, err = capsys.readouterr()
    assert "usage: apigen [-h]" in out
    assert err == ""


def test_shows_version(capsys):
    with pytest.raises(SystemExit):
        main.main(["--version"])
    out, err = capsys.readouterr()
    assert version.version in out
    assert err == ""


class NoOp(Subcommand):
    def run(self):
        pass


@pytest.mark.parametrize("definition", ["5", "D", "DEBUG"])
def test_sets_verbosity(definition):
    parse_args(["--verbosity", definition, "noop"], [NoOp])
    assert logger.level == logging.DEBUG


def test_default_logging():
    parse_args(["noop"], [NoOp])
    assert logger.level == 20
    assert logger.handlers[0].stream == sys.stderr


def test_supports_logging_to_stdout(mh):
    config_file = mh.write_config(logging__logfile="-")
    parse_args(["-c", str(config_file), "noop"], [NoOp])
    assert logger.handlers[0].stream == sys.stdout


def test_supports_logging_to_file(mh):
    logfile = mh.tmp_path / "output.log"
    config_file = mh.write_config(
        logging__loglevel="WARNING",
        logging__logfile=str(logfile))
    parse_args(["-c", str(config_file), "noop"], [NoOp])
    assert logger.level == logging.WARNING
    assert logger.handlers[0].stream.name == str(logfile)


def test_disapproves_of_non_writable_log(mh, lg):
    config_file = mh.write_config(logging__logfile="/not/even/writable")
    parse_args(["-c", str(config_file), "noop"], [NoOp])
    assert "No such file or directory: '/not/even/writable'" in lg.errors[0]
    assert logger.handlers[0].stream == sys.stderr


def test_reads_default_config_file(mh, in_tmpdir):
    mh.write_config(logging__loglevel="WARNING")
    parse_args(["noop"], [NoOp])
    assert logger.level == logging.WARNING
