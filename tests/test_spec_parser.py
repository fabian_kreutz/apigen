import enum
import json
import pytest
import yaml
from apigen.parsers.spec import SpecParser
from apigen.spec import OpenApiV3Spec, AsyncApiV2Spec
from apigen.spec.common import make_security_scheme_class
from helpers.spec import min_openapi_v3, min_asyncapi_v2


@pytest.mark.parametrize("filename,content,spectype", [
    ("openapi.json", min_openapi_v3, OpenApiV3Spec),
    ("openapi.fizzbuzz", min_openapi_v3, OpenApiV3Spec),
    ("openapi.yaml", yaml.dump(min_openapi_v3), OpenApiV3Spec),
    ("openapi.fizzbuzz", yaml.dump(min_openapi_v3), OpenApiV3Spec),
    ("asyncapi.json", min_asyncapi_v2, AsyncApiV2Spec)])
def test_identifies_openapi_v3(tmp_path, filename, content, spectype):
    api_file = tmp_path / filename
    if isinstance(content, dict):
        content = json.dumps(content)
    api_file.write_text(content)
    spec = SpecParser().parse(api_file)
    assert isinstance(spec, spectype)
    assert spec.info.title == "Test API"


@pytest.mark.parametrize("suffix,content", [
    ("yaml", "just a string"),
    ("json", "[]")])
def test_complains_about_invalid_input(tmp_path, suffix, content):
    api_file = tmp_path / f"api.{suffix}"
    api_file.write_text(content)
    with pytest.raises(
            Exception,
            match=f"api.{suffix}`: File does not contain a structure"):
        SpecParser().parse(api_file)


@pytest.mark.parametrize("content,error", [
    ('{"openapi": "2.0.0"}', "Unsupported openapi version 2.0.0"),
    ('{"swagger": "2.0.4"}', "File contains outdated `swagger` format"),
    ('{"asyncapi": "1.0.0"}', "Unsupported asyncapi version 1.0.0"),
    ('{}', "File does not seem to contain an API definition")])
def test_complains_about_invalid_input_format(tmp_path, content, error):
    api_file = tmp_path / "api.json"
    api_file.write_text(content)
    with pytest.raises(Exception, match=f"api.json`: {error}"):
        SpecParser().parse(api_file)


@pytest.mark.parametrize("spec", [min_openapi_v3, min_asyncapi_v2])
def test_full_api_info(spec):
    content = spec.copy()
    content["info"] = {
        "title": "Test API",
        "version": "1.2.3",
        "description": "This spec *is* for testing",
        "termsOfService": "https://terms.example.com",
        "contact": {
            "name": "Mr. C. Lens",
            "url": "https://myspace/lens",
            "email": "lens@aol.com"},
        "license": {
            "name": "MIT License",
            "url": "https://licenses.com/mit/"}}
    spec = SpecParser().parse_structure(content)
    assert spec.info.description_md == \
        "<p>This spec <em>is</em> for testing</p>"
    assert spec.info.contact.name == "Mr. C. Lens"
    assert spec.info.license.name == "MIT License"


@pytest.mark.parametrize("spec", [min_openapi_v3, min_asyncapi_v2])
def test_full_api_tags(spec):
    content = spec.copy()
    content["tags"] = [{
        "name": "physical",
        "description": "Stuff you can touch",
        "externalDocs": {
            "url": "https://docs.example.com",
            "description": "API spec doc"}}]
    spec = SpecParser().parse_structure(content)
    assert spec.tags[0].description == "Stuff you can touch"


@pytest.mark.parametrize("spec", [min_openapi_v3, min_asyncapi_v2])
def test_full_api_main_externalDocs(spec):
    content = spec.copy()
    content["externalDocs"] = {
        "url": "https://docs.example.com",
        "description": "API spec doc"}
    spec = SpecParser().parse_structure(content)
    assert spec.externalDocs.url == "https://docs.example.com"


class SecurityTypeEnum(enum.Enum):
    password = "password"


def test_pydantic_mypy_type_schenanigans_on_security_scheme():
    # I was unable to satisfy mypy and had to disable type checks there
    # As an alternative this test ensures that the types are checked.
    SecuritySchemeObject = make_security_scheme_class(SecurityTypeEnum)
    assert SecuritySchemeObject(type="password").type.value == "password"
    with pytest.raises(ValueError, match="not a valid enumeration member"):
        SecuritySchemeObject(type="invalid")
    with pytest.raises(AssertionError, match="Invalid type_enum class:"):
        make_security_scheme_class(OpenApiV3Spec)


def test_url_validator_does_not_choke_on_explicit_null():
    # If the dict does not contain the key and the value is Optional
    # then pydantic does not call the validator.  But if a value
    # is explicitly given in the dict, the validator is always called.
    content = min_openapi_v3.copy()
    content["info"]["contact"] = {"name": "Bob", "url": None}
    spec = SpecParser().parse_structure(content)
    assert spec.info.contact.url is None


def test_spec_has_acronym():
    spec = SpecParser().parse_structure(min_openapi_v3.copy())
    assert spec.acronym == "TAPI"
