import pytest
from apigen.parsers.spec import SpecParser
from helpers.spec import make_openapi_spec as make_spec


def parse_structure(structure):
    return SpecParser().parse_structure(structure)


def test_full_openapi_servers():
    content = make_spec()
    content["servers"] = [{
        "url": "https://server.url",
        "description": "The first server",
        "variables": {"flower": {
            "default": "clove",
            "description": "Beauty on the table",
            "enum": ["clove", "sunflower", "tulip"]}}}, {
        "url": "https://server2.url",
        "description": "The second server"}]
    spec = parse_structure(content)
    assert spec.servers[0].variables["flower"].enum[1] == "sunflower"
    assert spec.servers[1].description == "The second server"


def test_all_openapi_components():
    content = make_spec()
    content["components"] = {
        "schemas": {"key": {"type": "string"}},
        "responses": {"success": {"description": "this response"}},
        "parameters": {"key": {
            "name": "param", "in": "query",
            "schema": {"type": "string"}}},
        "examples": {"key": {"description": "this example"}},
        "requestBodies": {"key": {"content": {}}},
        "headers": {"key": {"schema": {"type": "integer"}}},
        "securitySchemes": {"key": {"type": "http", "scheme": "s"}},
        "links": {"key": {"operationId": "someId"}},
        "callbacks": {"key": {"description": "this callback"}}}
    spec = parse_structure(content).components
    assert spec.schemas["key"].type == "string"
    assert spec.responses["success"].description == "this response"
    assert spec.parameters["key"].name == "param"
    assert spec.examples["key"].description == "this example"
    assert spec.requestBodies["key"].content == {}
    assert spec.headers.__root__["key"].schema_.type == "integer"
    assert spec.securitySchemes["key"].type.value == "http"
    assert spec.links["key"].operationId == "someId"
    assert spec.callbacks["key"].description == "this callback"


def test_full_openapi_security():
    content = make_spec()
    content["security"] = {"pet_store": ["write:pets", "read:pets"]}
    spec = parse_structure(content)
    assert spec.security == {"pet_store": ["write:pets", "read:pets"]}


def test_full_openapi_paths():
    content = make_spec()
    minimum_op = content["paths"]["/trigger_event"]["post"]
    content["paths"]["/api"] = {
        "summary": "API summary",
        "description": "API description",
        "get": minimum_op,
        "put": minimum_op,
        "post": minimum_op,
        "delete": minimum_op,
        "options": minimum_op,
        "head": minimum_op,
        "patch": minimum_op,
        "trace": minimum_op,
        "servers": [{"url": "https://server.com"}],
        "parameters": [
            {"name": "param", "in": "query",
             "schema": {"type": "string"}}]}
    spec = parse_structure(content)
    assert 200 in spec.paths["/api"].head.responses


def test_operations_in_openapi_paths():
    content = make_spec()
    minimum_op = content["paths"]["/trigger_event"]["post"]
    content["paths"]["/api"] = {
        "put": minimum_op,
        "options": minimum_op,
        "trace": minimum_op}
    spec = parse_structure(content)
    assert list(spec.paths["/api"].operations.keys()) == [
        "put", "options", "trace"]
    assert 200 in spec.paths["/api"].operations["put"].responses


def test_single_operation_internals():
    content = make_spec()
    minimum_op = content["paths"]["/trigger_event"]["post"]
    content["paths"]["/trigger_event"]["post"] = {
        "responses": {},
        "tags": ["mytag"],
        "summary": "Trigger the event",
        "description": "Sample description",
        "externalDocs": {"url": "https://docs.example.com"},
        "operationId": "POST_trigger_event",
        "parameters": [
            {"name": "param", "in": "query",
             "schema": {"type": "string"}}],
        "requestBody": {
            "description": "Body data",
            "required": True,
            "content": {
                "application/json": {
                    "schema": {"type": "string"}}}},
        "callbacks": {"myC": {'/back-at-you': {"post": minimum_op}}},
        "security": [{"petstore_auth": ["write:pet"]}],
        "servers": [{"url": "https://server.com"}],
        "deprecated": True}
    pspec = parse_structure(content).paths["/trigger_event"].post
    assert "application/json" in pspec.requestBody.content
    assert type(pspec.callbacks["myC"]["/back-at-you"].post) is type(pspec)


def test_operation_request_body_content():
    content = make_spec()
    content["paths"]["/trigger_event"]["post"]["requestBody"] = {
        "content": {"application/json": {
            "schema": {
                "type": "object",
                "properties": {
                    "firstname": {"type": "string"},
                    "middlename": {"type": "object", "properties": {}},
                    "lastname": {"type": "string", "format": "binary"},
                    "address": {"type": "string"}}},
            "examples": {"bee": {
                "summary": "summ summ summ",
                "description": "the description",
                "value": "anything",
                "externalValue": "revelation"}},
            "encoding": {
                "firstname": {
                    "headers": {
                        "SERVERVERSION": {"schema": {"type": "string"}}},
                    "style": "form",
                    "allowReserved": True},
                "middlename": {},
                "lastname": {},
                "address": {"contentType": "application/pdf"}}}}}
    pspec = parse_structure(content).paths["/trigger_event"].post\
        .requestBody.content["application/json"]
    assert pspec.schema_.type == "object"
    assert pspec.examples["bee"].value == "anything"
    # automatic defaults depending on propertytype:
    assert pspec.encoding["firstname"].explode is True
    assert pspec.encoding["firstname"].contentType == "text/plain"
    assert pspec.encoding["firstname"].style.value == "form"
    assert pspec.encoding["middlename"].contentType == "application/json"
    assert pspec.encoding["lastname"].explode is False
    assert pspec.encoding["lastname"].contentType == "application/octet-stream"
    assert pspec.encoding["address"].contentType == "application/pdf"
    assert "SERVERVERSION" in pspec.encoding["firstname"].headers.__root__


def test_operation_request_body_encoding_must_refer_to_schema_property1():
    content = make_spec()
    content["paths"]["/trigger_event"]["post"]["requestBody"] = {
        "content": {"application/json": {
            "schema": {"type": "string"},
            "encoding": {"money": {}}}}}
    with pytest.raises(
            ValueError, match="Encodings must refer to schema properties"):
        parse_structure(content)


def test_operation_request_body_encoding_must_refer_to_schema_property2():
    content = make_spec()
    content["paths"]["/trigger_event"]["post"]["requestBody"] = {
        "content": {"application/json": {
            "schema": {"type": "object"},
            "encoding": {"money": {}}}}}
    with pytest.raises(
            ValueError, match="Encoding refers to unknown property money"):
        parse_structure(content)


def test_media_type_example_and_examples_mutually_exclusive():
    content = make_spec()
    content["paths"]["/trigger_event"]["post"]["requestBody"] = {
        "content": {"application/json": {
            "schema": {"type": "str"},
            "example": "anything",
            "examples": {"cat": {}}}}}
    with pytest.raises(
            ValueError,
            match=r"-> example\s+is mutually exclusive with `examples`"):
        parse_structure(content)


def test_only_example_case_for_coverage():
    content = make_spec()
    content["paths"]["/trigger_event"]["post"]["requestBody"] = {
        "content": {"application/json": {
            "schema": {"type": "str"},
            "example": "anything"}}}
    parse_structure(content)


def test_full_response_object():
    content = make_spec()
    content["paths"]["/trigger_event"]["post"]["responses"]["200"] = {
        "description": "Response description",
        "headers": {"SERVERVERSION": {"schema": {"type": "string"}}},
        "content": {"application/json": {"schema": {"type": "string"}}},
        "links": {
            "here": {"operationRef": "someRef"},
            "there": {
                "operationId": "someId",
                "parameters": {"param": "anything"},
                "requestBody": "anything",
                "description": "There description",
                "server": {"url": "https://there.com"}}}}
    pspec = parse_structure(content).paths["/trigger_event"].post\
        .responses[200]
    assert pspec.links["there"].server.url == "https://there.com"
    assert pspec.headers.__root__["SERVERVERSION"].name == "SERVERVERSION"


def test_link_object_without_reference():
    content = make_spec()
    content["paths"]["/trigger_event"]["post"]["responses"]["200"] = {
        "description": "Success",
        "links": {"bad": {"requestBody": 123}}}
    with pytest.raises(
            ValueError,
            match="Exactly one of operationId, operationRef must be given"):
        parse_structure(content)


def test_link_object_with_two_references():
    content = make_spec()
    content["paths"]["/trigger_event"]["post"]["responses"]["200"] = {
        "description": "Success",
        "links": {"bad": {"operationRef": "someRef", "operationId": "someId"}}}
    with pytest.raises(
            ValueError,
            match="Exactly one of operationId, operationRef must be given"):
        parse_structure(content)


def test_apispec_is_extensible():
    content = make_spec()
    content["x-goodVibrations"] = 5
    spec = parse_structure(content)
    assert spec.extensions == {"goodVibrations": 5}


@pytest.mark.parametrize("sstype,extra_data", [
        ("apiKey", {"name": "token", "in": "query"}),
        ("http", {"scheme": "basic"}),
        ("http", {"scheme": "bearer", "bearerFormat": "base64"}),
        ("oauth2", {"flows": {"password": {
            "tokenUrl": "https://token",
            "scopes": {"write:pet": "Write permission on pets"}}}}),
        ("openIdConnect", {"openIdConnectUrl": "https://connect.url"}),
    ])
def test_valid_security_schemes(sstype, extra_data):
    content = make_spec()
    sscheme = {"type": sstype, "description": "security scheme description"}
    sscheme.update(extra_data)
    content["components"] = {"securitySchemes": {f"{sstype}Auth": sscheme}}
    spec = parse_structure(content).components
    assert spec.securitySchemes[f"{sstype}Auth"].type.value == sstype


@pytest.mark.parametrize("sstype,missing", [
    ("apiKey", "name"),
    ("http", "scheme"),
    ("oauth2", "flows"),
    ("openIdConnect", "openIdConnectUrl")])
def test_invalid_security_schemes(sstype, missing):
    content = make_spec()
    content["components"] = {
        "securitySchemes": {f"{sstype}Auth": {"type": sstype}}}
    with pytest.raises(ValueError, match=f"{missing}\n  field required"):
        parse_structure(content)


@pytest.mark.parametrize("flowtype,missing,given", [
    ("password", "tokenUrl", {}),
    ("clientCredentials", "tokenUrl", {}),
    ("implicit", "authorizationUrl", {}),
    ("authorizationCode", "tokenUrl", {"authorizationUrl": "u"}),
    ("authorizationCode", "authorizationUrl", {"tokenUrl": "u"})])
def test_invalid_auth_security_flows(flowtype, missing, given):
    content = make_spec()
    flow = {"scopes": {"a": "b"}}
    flow.update(given)
    content["components"] = {
        "securitySchemes": {"myAuth": {
            "type": "oauth2",
            "flows": {flowtype: flow}}}}
    with pytest.raises(
            ValueError, match=f"{flowtype} -> {missing} is required"):
        parse_structure(content)


def test_not_existing_flowtype_for_coverage():
    content = make_spec()
    content["components"] = {
        "securitySchemes": {"myAuth": {
            "type": "oauth2",
            "flows": {"weirdness": {"scopes": {"a": "b"}}}}}}
    with pytest.raises(
            ValueError, match="value is not a valid enumeration member"):
        parse_structure(content)


def test_full_parameter_object():
    content = make_spec()
    content["paths"]["/trigger_event"]["parameters"] = [
        {"name": "param",
         "in": "query",
         "description": "parameter description",
         "required": True,
         "deprecated": True,
         "allowEmptyValue": True,
         "schema": {"type": "integer"},
         "style": "simple",
         "explode": True,
         "allowReserved": True,
         "example": "anything"}]
    pspec = parse_structure(content).paths["/trigger_event"].parameters
    assert pspec[0].name == "param"


def test_parameter_default_values():
    content = make_spec()
    content["paths"]["/trigger_event"]["parameters"] = [
        {"name": "param", "in": "path", "schema": {"type": "string"}},
        {"name": "param", "in": "query", "schema": {"type": "string"}}]
    pspec = parse_structure(content).paths["/trigger_event"].parameters
    assert pspec[0].required is True
    assert pspec[0].style.value == "simple"
    assert pspec[0].explode is False
    assert pspec[1].required is False
    assert pspec[1].style.value == "form"
    assert pspec[1].explode is True


def test_parameter_needs_schema_or_content():
    content = make_spec()
    content["paths"]["/trigger_event"]["parameters"] = [
        {"name": "param", "in": "path"}]
    with pytest.raises(
            ValueError, match="Exactly one of content, schema must be given"):
        parse_structure(content)


def test_parameter_can_have_schema_or_content():
    content = make_spec()
    content["paths"]["/trigger_event"]["parameters"] = [{
        "name": "param", "in": "path",
        "schema": {"type": "string"},
        "content": {"text/plain": {"schema": {"type": "string"}}}}]
    with pytest.raises(
            ValueError, match="Exactly one of content, schema must be given"):
        parse_structure(content)


def test_parameter_examples_mutually_exclusive():
    content = make_spec()
    content["paths"]["/trigger_event"]["parameters"] = [{
        "name": "param", "in": "path", "schema": {"type": "string"},
        "example": "anything", "examples": {"cat": {}}}]
    with pytest.raises(
            ValueError,
            match="example\n  is mutually exclusive with `examples`"):
        parse_structure(content)


@pytest.mark.parametrize("key", ["name", "in"])
def test_header_with_parameter_key(key):
    content = make_spec()
    content["components"] = {"headers": {"hname": {key: "form"}}}
    with pytest.raises(ValueError, match="extra fields not permitted"):
        parse_structure(content)


def test_header_with_non_dict_for_coverage():
    content = make_spec()
    content["components"] = {"headers": {"hname": 123}}
    with pytest.raises(ValueError, match="value is not a valid dict"):
        parse_structure(content)


def parse_schema(schema, components=None):
    content = make_spec()
    content["paths"]["/trigger_event"]["post"]["requestBody"] = {
        "content": {"application/json": {"schema": schema}}}
    if components:
        content["components"] = components
    return parse_structure(content).paths["/trigger_event"].post\
        .requestBody.content["application/json"].schema_


def test_minimum_schema_object_types():
    schema = parse_schema({
        "type": "object",
        "properties": {
            "a_string": {
                "type": "string"},
            "an_integer": {
                "type": "integer"},
            "a_float": {
                "type": "number"},
            "a_bool": {
                "type": "boolean"},
            "an_array": {
                "type": "array",
                "items": {"type": "integer"}},
            "a_null": {
                "type": "null",
                "nullable": True}}})
    assert schema.properties["a_null"].default is None


def test_all_full_schema_object_types():
    schema = parse_schema({
        "type": "object",
        "properties": {
            "a_string": {
                "type": "string",
                "default": "strrrr",
                "maxLength": 20,
                "minLength": 2,
                "pattern": "[a-z]+r"},
            "an_integer": {
                "type": "integer",
                "default": 12,
                "multipleOf": 4,
                "maximum": 120,
                "exclusiveMaximum": 121,
                "minimum": 4,
                "exclusiveMinimum": 3},
            "a_float": {
                "type": "number",
                "default": 12.8,
                "multipleOf": 4.2,
                "maximum": 120.8,
                "exclusiveMaximum": 120.9,
                "minimum": 4.0,
                "exclusiveMinimum": 3.99},
            "a_bool": {
                "type": "boolean",
                "default": True},
            "an_array": {
                "type": "array",
                "default": [1, 2, 3],
                "items": {"type": "integer"},
                "maxItems": 99,
                "minItems": 2,
                "uniqueItems": True},
            "a_null": {
                "type": "null",
                "nullable": True,
                "default": None}},
        "default": {"a_string": "blarrr"},
        "maxProperties": 14,
        "minProperties": 1,
        "required": ["a_string"],
        "additionalProperties": True})
    assert schema.properties["a_null"].default is None


def test_full_schema_object():
    schema = parse_schema({
        "type": "string",
        "title": "a title",
        "description": "the description",
        "format": "password",
        "enum": ["val1", "val2"],
        "externalDocs": {"url": "https://docs.example.com"},
        "example": "any value",
        "deprecated": True,
        "readOnly": False,
        "writeOnly": False,
        "xml": {
            "name": "TitleInCamelcase",
            "namespace": "com.lets.make.the.name.long",
            "prefix": "string_",
            "attribute": True,
            "wrapped": True}})
    assert schema.enum[1] == "val2"


@pytest.mark.parametrize("combinator", ["allOf", "oneOf", "anyOf"])
def test_combinatoric_schema_object(combinator):
    schema = parse_schema({
        combinator: [
            {"type": "integer", "minimum": 3},
            {"type": "integer", "maximum": 5}],
        "discriminator": {
            "propertyName": "petType",
            "mapping": {"frankenstein": "doctor"}}})
    assert getattr(schema, combinator)[0].type == "integer"


def test_negative_combinatoric_schema_object():
    schema = parse_schema({
        "allOf": [
            {"type": "string"},
            {"not": {"type": "boolean"}}]})
    assert schema.allOf[1].not_.type == "boolean"


def test_uses_references():
    schema = parse_schema(
        {"$ref": "#/components/schemas/Pet"},
        {"schemas": {"Pet": {"type": "string", "description": "a pet"}}})
    assert schema.description == "a pet"
    assert schema.referenceName == "schemas/Pet"


@pytest.mark.parametrize("ref,error", [
    ("https:/elsewhere.com/schemas/petstore#Pet",
     "Only internal references supported"),
    ("#/components/",
     "Unsupported reference #/components/"),
    ("#/components/schemas/simply_not_there",
     "Undefined reference #/components/schemas/simply_not_there")])
def test_invalid_references(ref, error):
    with pytest.raises(ValueError, match=error):
        parse_schema({"$ref": ref})


def test_enriched_reference_not_supported():
    with pytest.raises(
            ValueError, match=r"No attributes next to \$ref allowed"):
        parse_schema(
            {"$ref": "#/components/schemas/Pet",
             "description": "MY pet"},
            {"schemas": {"Pet": {"type": "string"}}})


def test_fails_on_description_md_get_without_description_for_coverage():
    content = make_spec()
    content["info"]["contact"] = {"name": "me"}
    spec = parse_structure(content)
    with pytest.raises(AttributeError):
        spec.info.contact.description_md


def test_operation_with_non_oauth_security():
    content = make_spec()
    content["components"] = {
        "securitySchemes": {
            "petstore_auth": {
                "type": "http",
                "scheme": "s"}}}
    content["paths"]["/trigger_event"]["post"]["security"] = [
        {"petstore_auth": ["write:pet"]}]
    spec = parse_structure(content)
    op = spec.paths["/trigger_event"].post
    assert list(op.oauth_scopes(spec.components.securitySchemes)) == []


def test_operation_with_unknown_security():
    content = make_spec()
    content["components"] = {"securitySchemes": {}}
    content["paths"]["/trigger_event"]["post"]["security"] = [
        {"petstore_auth": ["write:pet"]}]
    spec = parse_structure(content)
    with pytest.raises(
            ValueError,
            match="Operation requires unknown security scheme petstore_auth"):
        list(spec.paths["/trigger_event"].post
             .oauth_scopes(spec.components.securitySchemes))


def test_operation_with_oauth_without_flows():
    content = make_spec()
    content["components"] = {
        "securitySchemes": {
            "petstore_auth": {
                "type": "oauth2",
                "flows": {
                    "password": {
                        "tokenUrl": "https://token.com",
                        "scopes": {"read:pet": "read desc"}}}}}}
    content["paths"]["/trigger_event"]["post"]["security"] = [
        {"petstore_auth": ["write:pet"]}]
    spec = parse_structure(content)
    op = spec.paths["/trigger_event"].post
    assert list(op.oauth_scopes(spec.components.securitySchemes)) == []


def test_combinatoric_schema_with_too_few_elements():
    content = make_spec()
    content["components"] = {"schemas": {
        "testSchema": {"allOf": [{"type": "integer"}]}}}
    with pytest.raises(ValueError, match="must have more than one element"):
        parse_structure(content)


def test_combinatoric_schema_with_conflicting_types():
    content = make_spec()
    content["components"] = {"schemas": {
        "testSchema": {"allOf": [{"type": "string"}, {"type": "integer"}]}}}
    with pytest.raises(ValueError, match="allOf with conflicting types"):
        parse_structure(content)


def test_double_combinatoric_schema():
    content = make_spec()
    content["components"] = {"schemas": {
        "testSchema": {
            "allOf": [
                {"anyOf": [{"type": "string"}, {"type": "integer"}]},
                {"oneOf": [{"type": "string"}, {"type": "integer"}]},
            ]}}}
    with pytest.raises(ValueError, match="too complicated"):
        parse_structure(content)


def test_children_of_not_combinator():
    content = make_spec()
    content["components"] = {"schemas": {
        "testSchema": {"not": {"type": "integer"}}}}
    spec = parse_structure(content).components.schemas["testSchema"]
    assert spec.children[0].type == "integer"


def test_schema_without_valid_type():
    content = make_spec()
    content["components"] = {"schemas": {
        "testSchema": {}}}
    with pytest.raises(
            ValueError, match="testSchema -> type\n  field required"):
        parse_structure(content)


def test_parameter_with_mediatype():
    content = make_spec()
    content["components"] = {"parameters": {
        "testParam": {
            "name": "complicated",
            "in": "header",
            "content": {
                "application/json": {"schema": {"type": "string"}}}}}}
    spec = parse_structure(content).components.parameters["testParam"]
    assert spec.content["application/json"].schema_.type == "string"


def test_parameter_as_reference():
    content = make_spec()
    content["paths"]["/trigger_event"]["post"]["parameters"] = [
        {"$ref": "#/components/parameters/testParam"}]
    content["components"] = {"parameters": {
        "testParam": {
            "name": "simple",
            "in": "header",
            "schema": {"type": "string"}}}}
    op = parse_structure(content).paths["/trigger_event"].post
    assert op.parameters[0].name == "simple"


def test_parameters_can_have_only_one_mediatype():
    content = make_spec()
    content["components"] = {"parameters": {
        "testParam": {
            "name": "complicated",
            "in": "header",
            "content": {
                "application/json": {"schema": {"type": "string"}},
                "application/yaml": {"schema": {"type": "string"}}}}}}
    with pytest.raises(ValueError, match="can contain only one mediatype"):
        parse_structure(content)


def test_combinatoric_schema_can_be_a_reference():
    content = make_spec()
    content["components"] = {"schemas": {"SuccessResponse": {
        "oneOf": [{"type": "integer"}, {"type": "string"}]}}}
    op = content["paths"]["/trigger_event"]["post"]
    op["responses"]["200"]["content"] = {
        "application/json": {"schema": {
            "$ref": "#/components/schemas/SuccessResponse"}}}
    spec = parse_structure(content).paths["/trigger_event"].post
    schema = spec.responses[200].content["application/json"].schema_
    assert schema.type == "combinator"
    assert len(schema.oneOf) == 2


def test_recursive_schema_definition():
    schema = parse_schema(
        {"$ref": "#/components/schemas/Pet"},
        {"schemas": {
            "Pet": {
                "type": "object",
                "properties": {
                    "name": {"type": "string"},
                    "children": {
                        "type": "array",
                        "items": {"$ref": "#/components/schemas/Pet"}}}}}})
    assert schema.referenceName == "schemas/Pet"
    assert schema.properties["children"].items == schema
