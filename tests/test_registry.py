import pytest
from unittest.mock import patch, sentinel
from apigen import main
from apigen.registry import registry
from apigen.generators import BaseGenerator
from apigen.parsers.spec import SpecParser


class FakeGenerator(BaseGenerator):
    target_suffix = "fake"

    def render(self, spec):
        return f"Rendered output of {spec}"


class FakeEntryPoint:
    def __init__(self, name, target_instance=None):
        self.name = name
        self.target_instance = target_instance or FakeGenerator()

    def load(self):
        if isinstance(self.target_instance, Exception):
            raise self.target_instance
        return self.target_instance


def test_finds_generators_from_entrypoints():
    with patch("apigen.registry.entry_points") as mock:
        mock.return_value = [
            FakeEntryPoint("html"),
            FakeEntryPoint("python")]
        languages = registry.find_entrypoints("generators")
    assert sorted(languages.keys()) == ["html", "python"]


def test_crashes_on_invalid_generators(capsys):
    with patch("apigen.registry.entry_points") as mock:
        mock.return_value = [
            FakeEntryPoint("html"),
            FakeEntryPoint("weirdo", Exception("Too weird"))]
        with pytest.raises(SystemExit):
            registry.find_entrypoints("generators")
    _out, err = capsys.readouterr()
    assert "Problem loading class weirdo: Too weird" in err


def test_main_calls_generator(monkeypatch, tmp_path):
    input_file = tmp_path / "api.json"
    input_file.write_text("{}")
    monkeypatch.setattr(
        registry, "_generators", {"wuss": FakeGenerator})
    with patch.object(SpecParser, "parse", return_value=sentinel.api_spec):
        main.main(["generate", str(input_file), "-l", "wuss"])
    assert input_file.with_suffix(".fake").exists()
    assert input_file.with_suffix(".fake").read_text() == (
        "Rendered output of sentinel.api_spec")
