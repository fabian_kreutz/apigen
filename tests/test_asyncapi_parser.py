import pytest
from apigen.parsers.spec import SpecParser
from helpers.spec import make_asyncapi_spec as make_spec


def test_optional_asyncapi():
    content = make_spec()
    content["id"] = "urn:com:smartylighting:streetlights:server"
    content["defaultContentType"] = "application/json"
    spec = SpecParser().parse_structure(content)
    assert spec.id == "urn:com:smartylighting:streetlights:server"
    assert spec.defaultContentType == "application/json"


def test_id_must_be_urn():
    content = make_spec()
    content["id"] = "::/"
    with pytest.raises(ValueError, match="Not a valid url: ::/"):
        SpecParser().parse_structure(content)


def test_full_openapi_servers():
    content = make_spec()
    content["servers"] = {
        "prod": {
            "url": "https://server.url",
            "description": "The first server",
            "protocol": "http",
            "protocol_version": "5",
            "security": [{"pet_auth": ["read:pet", "write:pet"]}],
            "variables": {"flower": {
                "default": "clove",
                "description": "Beauty on the table",
                "enum": ["clove", "sunflower", "tulip"]}}},
        "stage": {
            "url": "https://server2.url",
            "protocol": "amqt",
            "security": []}}
    spec = SpecParser().parse_structure(content)
    assert spec.servers["prod"].variables["flower"].enum[1] == "sunflower"
    assert spec.servers["stage"].protocol == "amqt"
    assert spec.servers["prod"].security[0]["pet_auth"][0] == "read:pet"


def test_message_can_be_oneof():
    content = make_spec()
    content["channels"]["event"]["publish"]["message"] = {"oneOf": [
        {"payload": "abc"}, {"payload": 123}]}
    spec = SpecParser().parse_structure(content)
    assert len(spec.channels["event"].publish.message["oneOf"]) == 2


def test_message_can_not_be_all():
    content = make_spec()
    content["channels"]["event"]["publish"]["message"] = {"allOf": [
        {"payload": "abc"}, {"payload": 123}]}
    with pytest.raises(
            ValueError, match="Only oneOf combination allowed, not allOf"):
        SpecParser().parse_structure(content)


def test_full_asyncapi_components():
    content = make_spec()
    content["components"] = {
        "schemas": {
            "schema1": True,
            "schema2": False,
            "schema3": {"type": "str"}},
        "servers": {
            "server1": {"url": "https://there.com", "protocol": "http"}},
        "serverVariables": {
            "variable1": {"default": "standard"}},
        "channels": {
            "channel1": {"subscribe": {"message": {
                "$ref": "#/components/messages/m1"}}}},
        "messages": {
            "m1": {"payload": "anything"}},
        "securitySchemes": {
            "scheme1": {"type": "X509"}},
        "parameters": {
            "param1": {"schema": {"$ref": "#/components/schemas/schema3"}}},
        "correlationIds": {
            "id1": {"location": "$message.header"}},
        "operationTraits": {
            "optrait1": {"operationId": "abc"}},
        "messageTraits": {
            "msgtrait1": {"messageId": "bac"}},
        "serverBindings": {
            "sb1": {"jms": {"jmsConnectionFactory": "jcf"}}},
        "channelBindings": {
            "cb1": {"jms": {}, "anypointmq": {}}},
        "operationBindings": {
            "ob1": {"nats": {"query": "q"}}},
        "messageBindings": {
            "mb1": {"amqp": {"messageType": "mt"}}}}
    comps = SpecParser().parse_structure(content).components
    assert list(comps.channels["channel1"].operations.keys()) == ["subscribe"]
    assert comps.channels["channel1"].subscribe.message.payload == "anything"
    assert comps.parameters["param1"].schema_.type == "str"
    assert comps.operationBindings["ob1"].nats.query == "q"
    assert sorted(comps.channelBindings["cb1"]) == ["anypointmq", "jms"]


def test_full_channels():
    content = make_spec()
    content["channels"]["users"] = {
        "description": "Those *user* channels",
        "servers": ["production", "staging"],
        "subscribe": {
            "operationId": "subscribe_on_users",
            "summary": "summ summ summ",
            "description": "user subscribe description",
            "security": [{"user_auth": ["write:users"]}],
            "tags": [{"name": "users"}],
            "externalDocs": {"url": "https://api.com/user_subscribe"},
            "bindings": {"mqtt": {"qos": 2}},  # OperationBinding
            "message": {
                "messageId": "123",
                "headers": {"type": "str", "title": "Schema title"},
                "correlationId": {"location": "response"},
                "schemaFormat": "application/vnd.aai.avro+json",
                "contentType": "application/json",
                "name": "Message name",
                "title": "Message title",
                "summary": "Message summary",
                "description": "Message description",
                "tags": [{"name": "users"}],
                "externalDocs": {"url": "https://url.com/docs"},
                "payload": "anything",
                "traits": [{
                    "messageId": "t:123",
                    "headers": False,
                    "correlationId": {"location": "response"},
                    "schemaFormat": "application/vnd.aai.avro+json",
                    "contentType": "application/json",
                    "name": "t:Message name",
                    "title": "t:Message title",
                    "summary": "t:Message summary",
                    "description": "t:Message description",
                    "tags": [{"name": "t:users"}],
                    "externalDocs": {"url": "https://url.com/t:docs"}}]},
            "traits": [
                {"operationId": "t:subscribe_on_users",
                 "summary": "t:summ summ summ",
                 "description": "t:user subscribe description",
                 "security": [{"user_auth": ["t:write:users"]}],
                 "tags": [{"name": "t:users"}],
                 "externalDocs": {"url": "https://api.com/t:user_subscribe"},
                 "bindings": {"mqtt": {"qos": 1}}},
                {"operationId": "t2:subscribe_on_users"}]},
        "parameters": {
            "requestId": {
                "schema": {"type": "int"},
                "description": "RequestId parameter description",
                "location": "request"}},
        "bindings": {"ws": {"method": "get"}}}  # ChannelBinding
    chans = SpecParser().parse_structure(content).channels["users"]
    assert list(chans.operations.keys()) == ["subscribe"]
    assert chans.subscribe.summary == "t:summ summ summ"
    assert chans.subscribe.operationId == "t2:subscribe_on_users"
    assert chans.subscribe.message.messageId == "t:123"


def test_merging_traits():
    content = make_spec()
    content["channels"]["users"] = {
        "description": "Those *user* channels",
        "servers": ["production", "staging"],
        "subscribe": {
            "traits": [
                {"operationId": "subscribe_on_users"},
                {"externalDocs": {"url": "http://api.com"}}],
            "message": {
                "traits": [
                    {"messageId": "msg123"}
                ]
            }}}
    chans = SpecParser().parse_structure(content).channels["users"]
    assert chans.subscribe.operationId == "subscribe_on_users"
    assert chans.subscribe.message.messageId == "msg123"


def test_dont_choke_on_traits_with_reference():
    content = make_spec()
    content["components"] = {
        "messageTraits": {
            "msgTrait": {"name": "t:messageName"}}}
    content["channels"]["event"] = {
        "publish": {
            "message": {
                "messageId": "123",
                "traits": [{"$ref": "#/components/messageTraits/msgTrait"}]}}}
    chans = SpecParser().parse_structure(content).channels["event"]
    message = chans.publish.message
    assert message.messageId == "123"
    assert message.name == "t:messageName"
    assert message.referenceName is None


def test_merges_reference_traits_that_contain_more_references():
    content = make_spec()
    content["components"] = {
        "messages": {
            "myMessage": {
                "messageId": "123",
                "traits": [{"$ref": "#/components/messageTraits/msgTrait"}],
            }
        },
        "messageTraits": {
            "msgTrait": {"name": "t:messageName"}}}
    content["channels"]["event"] = {
        "publish": {
            "message": {"$ref": "#/components/messages/myMessage"}}}
    spec = SpecParser().parse_structure(content)
    for message in [
            spec.components.messages["myMessage"],
            spec.channels["event"].publish.message]:
        assert message.messageId == "123"
        assert message.name == "t:messageName"
