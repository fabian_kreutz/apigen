import json
import pytest
from apigen import main
from apigen.validate_command import Validate
from helpers.main import MainHelper
from helpers.spec import make_openapi_spec


@pytest.fixture
def mh(tmp_path, monkeypatch):
    helper = MainHelper(tmp_path, monkeypatch, Validate)
    with helper.run():
        yield helper


def test_requires_two_input_files(mh):
    api_file = mh.write("api.json", {})
    main.main(["validate", str(api_file), str(api_file)])
    mh.assert_call_argument(
        input_file=api_file, other_file=api_file,
        validator_config={
            "validate_array_order": False,
            "validate_references": False,
            "validate_ignore": []})


def test_requires_existing_input_file(mh, lg):
    api_file = mh.write("api.json", {})
    with pytest.raises(SystemExit):
        main.main(["validate", "/does/not/exist", str(api_file)])
    assert "File not found: `/does/not/exist`" in lg.errors


def test_requires_existing_other_file(mh, lg):
    api_file = mh.write("api.json", {})
    with pytest.raises(SystemExit):
        main.main(["validate", str(api_file), "/does/not/exist"])
    assert "File not found: `/does/not/exist`" in lg.errors


def test_validation_options_on_cli(mh):
    api_file = mh.write("api.json", {})
    main.main([
        "validate", str(api_file), str(api_file),
        "--validate-array-order", "-r"])
    mh.assert_call_argument(
        validator_config={
            "validate_array_order": True,
            "validate_references": True})


def test_validation_branches_on_cli(mh):
    api_file = mh.write("api.json", {})
    main.main([
        "validate", str(api_file), str(api_file),
        "--validate-ignore", "/servers", "-I", "/info/license"])
    mh.assert_call_argument(
        validator_config={
            "validate_ignore": ["/servers", "/info/license"]})


@pytest.mark.parametrize("value", [0, 1])
def test_validation_options_in_file(mh, value):
    api_file = mh.write("api.json", {})
    config_file = mh.write_config(
        validation__array_order=value,
        validation__references=value)
    main.main([
        "-c", str(config_file),
        "validate", str(api_file), str(api_file)])
    mh.assert_call_argument(
        validator_config={
            "validate_array_order": bool(value),
            "validate_references": bool(value)})


def test_validation_branches_in_file(mh):
    api_file = mh.write("api.json", {})
    config_file = mh.write_config(
        validation__ignore="\n  /servers\n  /info/license")
    print(config_file.read_text())
    main.main([
        "-c", str(config_file),
        "validate", str(api_file), str(api_file)])
    mh.assert_call_argument(
        validator_config={
            "validate_ignore": ["/servers", "/info/license"]})


def test_reports_no_spec_differences(tmp_path):
    api_file = tmp_path / "api.json"
    json.dump(make_openapi_spec(), api_file.open("wt"))
    with pytest.raises(SystemExit, match="0"):
        main.main(["validate", str(api_file), str(api_file)])


def test_reports_spec_differences(tmp_path, capsys):
    spec = make_openapi_spec()
    api_file1 = tmp_path / "api1.json"
    api_file2 = tmp_path / "api2.json"
    json.dump(spec, api_file1.open("wt"))
    spec["info"]["title"] = "Other API"
    json.dump(spec, api_file2.open("wt"))
    with pytest.raises(SystemExit, match="1"):
        main.main(["validate", str(api_file1), str(api_file2)])
    out = capsys.readouterr().out
    assert "Difference in  /info/title: Test API != Other API" in out
