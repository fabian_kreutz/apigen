from datetime import datetime as DateTime
from unittest.mock import patch
import pytest
from apigen.spec.openapi import SchemaObject


@pytest.mark.parametrize("schema", [
    {"enum": [3, 6, 9]},
    {"default": 3},
    {"example": 3},
    {"minimum": 3},
    {"maximum": 3},
    {"multipleOf": 3},
    {"exclusiveMinimum": 2},
    {"exclusiveMaximum": 4}])
def test_integer_examples(schema):
    obj = SchemaObject(type="integer", **schema)
    assert obj.make_example() == 3


def test_integer_default():
    obj = SchemaObject(type="integer")
    assert obj.make_example() == 1


@pytest.mark.parametrize("schema", [
    {"enum": [3.3, 6, 9]},
    {"default": 3.3},
    {"example": 3.3},
    {"minimum": 3.3},
    {"maximum": 3.3},
    {"multipleOf": 3.3},
    {"exclusiveMinimum": 3.1},
    {"exclusiveMaximum": 3.5}])
def test_number_examples(schema):
    obj = SchemaObject(type="number", **schema)
    assert round(obj.make_example(), 10) == 3.3


def test_number_default():
    obj = SchemaObject(type="number")
    assert obj.make_example() == 3.141


class FrozenDateTime:
    @staticmethod
    def now():
        return DateTime(2020, 10, 5, 12, 35)


@patch("apigen.spec.openapi.DateTime", FrozenDateTime)
@pytest.mark.parametrize("schema,example", [
    ({}, "value"),
    ({"format": "date-time", "default": "now"}, "2020-10-05T12:35:00Z"),
    ({"format": "date-time", "default": "5 minutes ago"},
        "2020-10-05T12:30:00Z"),
    ({"format": "date", "default": "now"}, "2020-10-05"),
    ({"format": "date", "default": "3 days ago"}, "2020-10-02"),
    ({"format": "date", "default": "in a fortnight"}, "in a fortnight"),
    ({"pattern": "^te?st*"}, "^te?st*"),
    ({"format": "byte"}, "\\x64"),
    ({"format": "binary"},
     "\\x76\\x111\\x114\\x101\\x109\\x32\\x105\\x112\\x115\\x117\\x109"),
    ({"format": "date"}, "1969-07-20"),
    ({"format": "date-time"}, "1969-07-20T09:32:00 EDT"),
    ({"format": "password"}, "********"),
    ({"format": "uuid"}, "d9a4f688-feff-49ea-978c-3621daabcf47"),
    ({"format": "regex"}, "^\\s*# (.*)$"),
    ({"format": "base64"}, "TG9yZW0gaXBzdW0="),
    ({"minLength": 5}, "aaaaa"),
    ])
def test_string_examples(schema, example):
    obj = SchemaObject(type="string", **schema)
    assert obj.make_example() == example


def test_boolean_example():
    obj = SchemaObject(type="boolean")
    assert obj.make_example() is True
    assert obj.render_example() == "true"


def test_array_example():
    obj = SchemaObject(type="array", items={"type": "string"})
    assert obj.make_example() == ["value"]


def test_object_example():
    obj = SchemaObject(type="object", properties=[])
    assert obj.make_example() == {"additionalProperty": "value"}


def test_object_example_with_additional_properties():
    obj = SchemaObject(type="object", additionalProperties={"type": "integer"})
    assert obj.make_example() == {"additionalProperty": 1}


def test_object_example_without_additional_properties():
    obj = SchemaObject(
        type="object", additionalProperties=False,
        properties={
            "name": {"type": "string", "example": "Sweet"},
            "age": {"type": "integer", "default": 17}})
    assert obj.make_example() == {
        "name": "Sweet",
        "age": 17}


def test_null_example():
    obj = SchemaObject(type="null", nullable=True)
    assert obj.make_example() is None
    assert obj.render_example() == "null"


@pytest.mark.parametrize("combinator", ["oneOf", "anyOf"])
def test_oneOf_or_anyOf_example(combinator):
    schema = {combinator: [
        {"type": "string", "example": "yes"},
        {"type": "boolean"},
    ]}
    obj = SchemaObject(**schema)
    assert obj.make_example() == "yes"


def test_allOf_example():
    schema = {"allOf": [
        {"type": "object", "additionalProperties": False, "properties": {
            "name": {"type": "string", "example": "Max"}}},
        {"type": "object", "additionalProperties": False, "properties": {
            "age": {"type": "integer"},
            "hair": {"type": "string", "default": "blonde"}}}]}
    obj = SchemaObject(**schema)
    assert obj.make_example() == {
        "name": "Max",
        "age": 1,
        "hair": "blonde"}


def test_not_example():
    assert SchemaObject(**{"not": {"type": "string"}}).make_example() == 1
    assert SchemaObject(**{"not": {"type": "integer"}})\
        .make_example() == "value"


def test_minimal_examples():
    assert SchemaObject(
        type="object",
        properties={"name": {"type": "string"}},
        additionalProperties=True).make_example(True) == {}
    assert SchemaObject(
        type="array",
        items={"type": "string"}).make_example(True) == []
    assert SchemaObject(
        type="array",
        items={"type": "string"},
        minItems=2).make_example(True) == ["value", "value"]
    assert SchemaObject(
        anyOf=[
            {"type": "integer"},
            {"type": "string"}]).make_example(True) == 1
    assert SchemaObject(
        type="object",
        required=["sub"],
        properties={"sub": {
            "oneOf": [
                {"type": "array", "items": {"type": "boolean"}},
                {"type": "string"}
            ]}}).make_example(True) == {"sub": []}
