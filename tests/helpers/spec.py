from copy import deepcopy


min_openapi_v3 = {
  "openapi": "3.0.1",
  "info": {"title": "Test API", "version": "1.2.3"},
  "paths": {"/trigger_event": {"post": {
      "responses": {"200": {"description": "Success"}}}}}
}
min_asyncapi_v2 = {
  "asyncapi": "2.4.0",
  "info": {"title": "Test API", "version": "1.2.3"},
  "channels": {"event": {"publish": {
      "message": {"payload": "anything"}}}}
}


def make_openapi_spec():
    return deepcopy(min_openapi_v3)


def make_asyncapi_spec():
    return deepcopy(min_asyncapi_v2)
