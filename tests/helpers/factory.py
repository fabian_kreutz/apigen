from apigen.spec import OpenApiV3Spec


def make_openapi_obj(operation, components={}, **kwargs):
    operation.setdefault("responses", {"200": {"description": "Success"}})
    return OpenApiV3Spec(
        type="openapi",
        api_version="3.0.1",
        info={"title": "Test API", "version": "1.2.3"},
        paths={"go/": {"get": operation}},
        components=components,
        **kwargs)


def minimal_struct():
    return {
        "openapi": "3.0.1",
        "info": {"title": "Test API", "version": "1.2.3"},
        "paths": {
            "/go": {
                "get": {
                    "responses": {"200": {"description": "Success"}}}}}}
