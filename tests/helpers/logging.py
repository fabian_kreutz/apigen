class LogBucket:
    def __init__(self, caplog):
        self.caplog = caplog
        self._records = None

    def records(self):
        if self._records is None:
            self._records = {}
            for rec in self.caplog.records:
                self._records.setdefault(rec.levelname, [])\
                    .append(rec.message)
        return self._records

    @property
    def errors(self):
        return self.records()["ERROR"]

    @property
    def warnings(self):
        return self.records()["WARNING"]

    @property
    def info(self):
        return self.records()["INFO"]

    def print_log(self):
        for level, messages in self.records().items():
            print(level)
            print("  " + "\n  ".join(messages))
            print()
