from configparser import ConfigParser
import json
from contextlib import contextmanager
from apigen import main


class MainHelper:
    def __init__(self, tmp_path, monkeypatch=None, command=""):
        self.tmp_path = tmp_path
        self.monkeypatch = monkeypatch
        self.command = command
        self.called_cmd = None

    def write(self, path, data=""):
        match path:
            case list():
                outfile = self.tmp_path.joinpath(*path)
            case str():
                outfile = self.tmp_path / path
            case _:
                raise TypeError("path is neither list nor string")
        outfile.parent.mkdir(parents=True, exist_ok=True)
        if outfile.suffix == ".json" and isinstance(data, dict):
            data = json.dumps(data)
        if isinstance(data, bytes):
            outfile.write_bytes(data)
        else:
            outfile.write_text(data)
        return outfile

    def write_config(self, name="apigen.ini", **config):
        cp = ConfigParser()
        for key, value in config.items():
            section, key = key.split("__")
            if section not in cp:
                cp.add_section(section)
            cp.set(section, key, str(value))
        config_file = self.tmp_path / name
        cp.write(config_file.open("wt"))
        return config_file

    @contextmanager
    def run(self):
        class DefangedCommand(self.command):
            def run(cmd):
                self.called_cmd = cmd

        DefangedCommand.__name__ = self.command.__name__
        self.monkeypatch.setattr(main, self.command.__name__, DefangedCommand)
        yield self

    def assert_call_argument(self, **kwargs):
        assert self.called_cmd is not None
        for key, expectation in kwargs.items():
            value = getattr(self.called_cmd, key)
            if key == "validator_config":
                for validation_key in expectation:
                    assert value[validation_key] == expectation[validation_key]
            else:
                assert value == expectation
