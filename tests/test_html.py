from pathlib import Path
import pytest
import re
from bs4 import BeautifulSoup
from apigen.generators.html import HtmlGenerator
from apigen.parsers.spec import SpecParser
from apigen.spec.openapi import OpenApiV3Spec


def strip_to(content, tag):
    if not tag:
        return content
    i1 = content.find(f"<{tag}")
    i2 = content.find(f"</{tag}>") + len(tag) + 3
    return content[i1:i2]


@pytest.fixture
def rh():
    unneccesary_whitespace = re.compile(r"(?<=>)\s+(?=<)", re.M | re.S)

    def render(generator_config={}, strip_tag="main", **spec_dict):
        spec_dict.setdefault("api_version", "3.0.1")
        spec_dict.setdefault("paths", {})
        spec_dict.setdefault("info", {"title": "Test API", "version": "1.2.3"})
        spec = OpenApiV3Spec(**spec_dict)
        result = HtmlGenerator(**generator_config).render(spec)
        result = strip_to(result, strip_tag)
        print(result)
        return unneccesary_whitespace.sub("", result)

    return render


def test_generates_from_given_template_directory(tmp_path, rh):
    template = tmp_path / "index.html.jinja"
    template.write_text(
        "{% include 'info.html.jinja' %}\n"
        "Custom template: {{spec.info.title}}")
    result = rh(
        generator_config={"template_dir": str(tmp_path)}, strip_tag=None)
    assert "Custom template: Test API" in result
    assert '<pre class="version">1.2.3</pre>' in result


def test_generates_from_included_templates(rh):
    result = rh(generator_config={"style": "basic"}, strip_tag="head")
    assert "<title>Test API 1.2.3 documentation</title>" in result


def test_rendition_shows_api_info(rh):
    result = rh(info={
        "title": "Test API",
        "version": "1.2.3",
        "description": "An *API* for testing",
        "termsOfService": "https://terms.test",
        "contact": {
            "name": "The Guru",
            "url": "https://gu.ru",
            "email": "gru@gu.ru"},
        "license": {
            "name": "MITtens",
            "url": "https://gu.ru/mittens"}},
        externalDocs={"url": "https://gu.ru/docs"})
    assert '<h1 class="title">Test API</h1>' in result
    assert '<pre class="version">1.2.3</pre>' in result
    assert '<p>An <em>API</em> for testing</p>' in result
    assert '<a href="https://terms.test">Terms of Service</a>' in result
    assert '<div class="contact_url"><a href="https://gu.ru">' in result
    assert '<div class="contact_email"><a href="mailto:gru@gu.ru">' in result
    assert '<a href="https://gu.ru/mittens">MITtens</a>' in result
    assert '<a href="https://gu.ru/docs">External Documentation</a>' in result


def test_rendition_of_minimum_api_info(rh):
    result = rh(info={
        "title": "Test API",
        "version": "1.2.3"})
    assert '</hgroup></div><hr /><div class="endpoints"></div></main>' \
        in result


def test_rendition_lists_spec_servers(rh):
    result = rh(servers=[
        {"url": "https://service.dev"},
        {"description": "staging",
         "url": "https://service.stage",
         "variables": {
             "debug": {
                 "default": "yes",
                 "description": "debug mode",
                 "enum": ["yes", "no"]}}},
        {"description": "production",
         "url": "https://service.prod"}])
    assert '<li><span class="url">https://service.dev</span></li>' in result
    assert '<span class="name">staging</span>: <span class="url">' in result


# tags[].externalDocs are not rendered
def test_navbar_by_tags(rh):
    result = rh(
        strip_tag="nav",
        tags=[
            {"name": "users",
             "description": "User operations",
             "externalDocs": {"url": "https://gu.ru/docs/users"}},
            {"name": "pets",
             "description": "Pet operations",
             "externalDocs": {"url": "https://gu.ru/docs/pets"}}],
        paths={
            "/users": {
                "get": {
                    "tags": ["users"],
                    "responses": {}},
                "post": {
                    "tags": ["users"],
                    "summary": "Send POST to user",
                    "responses": {}}},
            "/users/{id}/pets": {
                "get": {
                    "tags": ["pets"],
                    "responses": {}},
                "patch": {
                    "tags": ["pets"],
                    "summary": "PATCH up a pet",
                    "responses": {}}}})
    assert '<h2>users</h2>' in result
    assert '<div class="description"><p>User operations</p></div>' in result
    assert '<a href="#get_users" class="op_link">' in result
    assert '<span class="op op_get">get</span> /users' in result
    assert '<a href="#get_usersidpets" class="op_link">' in result
    assert '<span class="op op_get">get</span> /users/{id}/pets' in result


def test_navbar_without_tags(rh):
    result = rh(
        strip_tag="nav",
        paths={
            "/users": {
                "get": {
                    "responses": {}},
                "post": {
                    "summary": "Send POST to user",
                    "responses": {}}},
            "/users/{id}/pets": {
                "get": {
                    "responses": {}},
                "patch": {
                    "summary": "PATCH up a pet",
                    "responses": {}}}})
    assert '<a href="#get_users" class="op_link">' in result
    assert '<span class="op op_get">get</span> /users' in result
    assert '<a href="#get_usersidpets" class="op_link">' in result
    assert '<span class="op op_get">get</span> /users/{id}/pets' in result


# paths[].servers are not rendered
# paths[].op.externalDocs are not rendered
# paths[].op.servers are not rendered
# paths[].op.callbacks are not rendered  <- TODO?
def test_paths(rh):
    result = rh(
        components={
            "securitySchemes": {
                "pet_auth": {
                    "type": "oauth2",
                    "flows": {"password": {
                        "tokenUrl": "https://token.com",
                        "scopes": {"pet:read": "Right to read"}}}}}},
        paths={
            "/users/{id}/pets": {
                "summary": "CRUD on pets",
                "description": "This is *CRUD* on pets",
                "get": {
                    "tags": ["users", "pets"],
                    "summary": "The get of these ops",
                    "description": "This is *R* of the CRUD",
                    "operationId": "get_userpets",
                    "security": [{"pet_auth": ["pet:read"]}],
                    "parameters": [{
                        "name": "id",
                        "in": "path",
                        "schema": {"type": "integer"}}],
                    "responses": {"200": {
                        "description": "Success",
                        "content": {"text/plain": {
                            "schema": {"type": "string"}}}}}}},
            "/users/petsicles": {
                "get": {
                    "summary": "The old way",
                    "operationId": "get_petsicles",
                    "deprecated": True,
                    "responses": {"200": {
                        "description": "Success"}}}}})
    assert ('<a id="get_usersidpets"></a>'
            '<section class="operation">') in result
    assert ('<a id="get_userspetsicles"></a>'
            '<section class="operation deprecated">') in result
    assert ('<h4>Required scopes</h4>'
            '<ul><li><span class="scope_name">pet:read</span>'
            'Right to read</li></ul>') in result


def test_all_of_gets_combined(rh):
    def path_with_schema(schema):
        return {
            "/path": {
                "get": {
                    "responses": {
                        "400": {
                            "description": "Failure",
                            "content": {
                                "text/plain": {
                                    "schema": schema}}}}}}}

    result1 = rh(
        components={
            "schemas": {
                "GenericError": {
                    "type": "object",
                    "required": ["status", "message"],
                    "properties": {
                        "status": {"type": "string"},
                        "message": {"type": "string"},
                    }
                }
            }
        },
        paths=path_with_schema(
            {"allOf": [
                {"$ref": "#/components/schemas/GenericError"},
                {
                    "type": "object",
                    "required": ["code"],
                    "properties": {
                        "code": {"type": "integer"}
                    }
                }
            ]})
        )
    result2 = rh(
        paths=path_with_schema({
            "type": "object",
            "required": ["status", "message", "code"],
            "properties": {
                "status": {"type": "string"},
                "message": {"type": "string"},
                "code": {"type": "integer"}
            }
        })
    )
    result1 = BeautifulSoup(result1, "html.parser").main.section
    result2 = BeautifulSoup(result2, "html.parser").main.section
    assert result1.prettify() == result2.prettify()


def test_functional_interface_with_render_context(rh):
    result = rh(
        generator_config={
            "style": "basic",
            "render_context": {"base_url": "/somewhere/"}},
        strip_tag=None)
    assert 'href="/somewhere/resources/api.css"' in result
    assert 'src="/somewhere/resources/actionscripts.js"' in result


def test_cli_interface_to_render_context(rh):
    result = rh(
        generator_config={"rc.base_url": "/somewhere/"},
        strip_tag=None)
    assert 'href="/somewhere/resources/api.css"' in result
    assert 'src="/somewhere/resources/actionscripts.js"' in result


@pytest.mark.parametrize(
    "path", [str(p) for p in Path("tests", "regression").glob("*.json")])
def test_openapi_regression(path):
    path = Path(path)
    spec = SpecParser().parse(path)
    rendition = BeautifulSoup(HtmlGenerator().render(spec), "html.parser")
    exp_file = None
    for exp_file in path.parent.glob(path.stem + "*.html"):
        result = rendition
        expectation = exp_file.read_text()
        if expectation.startswith("<nav>"):
            result = rendition.body.nav
        elif expectation.startswith("<main "):
            result = rendition.body.main
        result = result.prettify()
        expectation = BeautifulSoup(expectation, "html.parser").prettify()
        assert result == expectation, result
    assert exp_file is not None, "No html expectation found"


def _test_render_recursive_object(rh):
    result = rh(
        components={
            "schemas": {
                "Entity": {
                    "type": "object",
                    "properties": {
                        "name": {"type": "string"},
                        "children": {
                            "type": "array",
                            "items": {
                                "$ref": "#/components/schemas/Entity"}}}}}},
        paths={
            "/users": {
                "get": {
                    "responses": {"200": {
                        "description": "Success",
                        "content": {"text/plain": {
                            "schema": {
                                "$ref": "#/components/schemas/Entity"}}}}}}}})
    print(result)
    assert False
