import pytest
from apigen.differ import SpecDiffer
from helpers.factory import make_openapi_obj


@pytest.mark.parametrize("val", [True, False])
def test_reference_validation(val):
    shared_schema = {
        "type": "object",
        "properties": {
            "name": {"type": "string"},
            "color": {"type": "string"}}}
    spec1 = make_openapi_obj({
        "requestBody": {
            "content": {
                "application/json": {
                    "schema": shared_schema}}}})
    spec2 = make_openapi_obj({
        "requestBody": {
            "content": {
                "application/json": {
                    "schema": {
                        "$ref": "#/components/schemas/Reffo"}}}}},
        components={
            "schemas": {
                "Reffo": shared_schema}})
    differ = SpecDiffer(validate_references=val)
    differences = differ.diff(spec1, spec2)
    assert bool(differences) is val, differences


@pytest.mark.parametrize("val", [True, False])
def test_parameter_array_order_validation(val):
    param1 = {"name": "name", "in": "query", "schema": {"type": "string"}}
    param2 = {"name": "color", "in": "query",  "schema": {"type": "string"}}
    spec1 = make_openapi_obj({"parameters": [param1, param2]})
    spec2 = make_openapi_obj({"parameters": [param2, param1]})
    differ = SpecDiffer(validate_array_order=val)
    differences = differ.diff(spec1, spec2)
    assert bool(differences) is val, differences


def make_combinatoric(*schemas):
    return {
        "requestBody": {
            "content": {
                "application/json": {
                    "schema": {"oneOf": list(schemas)}}}}}


@pytest.mark.parametrize("val", [True, False])
def test_combinatoric_array_order_validation(val):
    schema1 = {"type": "string"}
    schema2 = {"type": "integer"}
    spec1 = make_openapi_obj(make_combinatoric(schema1, schema2))
    spec2 = make_openapi_obj(make_combinatoric(schema2, schema1))
    differ = SpecDiffer(validate_array_order=val)
    differences = differ.diff(spec1, spec2)
    assert bool(differences) is val, differences


def test_combinatoric_array_mismatch():
    schema1 = {"type": "string"}
    schema2 = {"type": "integer"}
    schema3 = {"type": "boolean"}
    spec1 = make_openapi_obj(make_combinatoric(schema1, schema2, schema3))
    spec2 = make_openapi_obj(make_combinatoric(schema1, schema3))
    differ = SpecDiffer(validate_array_order=False)
    differences = differ.diff(spec1, spec2)
    assert "!= [{'type': 'string'}, None," in differences[0]


def test_fails_on_invalid_parameters():
    with pytest.raises(ValueError, match="Unknown option"):
        SpecDiffer(validate_this_and_that=True)


@pytest.mark.parametrize("ignores", [True, False])
@pytest.mark.parametrize("difference", "<>!")
def test_ignores_differences_in_ignored_branches(ignores, difference):
    spec1 = make_openapi_obj(
        {}, servers=[{"url": "test.io"}] if difference != ">" else [])
    spec2 = make_openapi_obj(
        {}, servers=[{"url": "io.test"}] if difference != "<" else [])
    differ = SpecDiffer(validate_ignore=["/servers"] if ignores else [])
    has_differences = bool(differ.diff(spec1, spec2))
    assert has_differences is not ignores
