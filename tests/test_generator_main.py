import json
from pathlib import Path
import pytest
import yaml
from apigen import main
from apigen.generate_command import Generate
from apigen.generators.html import HtmlGenerator
from helpers.main import MainHelper
from helpers.factory import minimal_struct


@pytest.fixture
def mh(tmp_path, monkeypatch):
    helper = MainHelper(tmp_path, monkeypatch, Generate)
    with helper.run():
        yield helper


def test_requires_only_input_file(mh):
    api_file = mh.write("api.json", {})
    main.main(["generate", str(api_file)])
    mh.assert_call_argument(
        input_file=api_file, output_file=api_file.with_suffix(".html"),
        generator_class=HtmlGenerator, generator_config={})


def test_missing_input_file_defaults_to_stdin(mh):
    main.main(["generate"])
    mh.assert_call_argument(
        input_file=Path("/dev/stdin"), output_file=Path("/dev/stdout"),
        generator_class=HtmlGenerator, generator_config={})


def test_supports_reading_stdin_writing_to_file(mh):
    outfile = mh.tmp_path.joinpath("output.html")
    main.main(["generate", "-", str(outfile)])
    mh.assert_call_argument(
        input_file=Path("/dev/stdin"), output_file=outfile,
        generator_class=HtmlGenerator, generator_config={})


def test_supports_reading_file_writing_to_stdout(mh):
    api_file = mh.write("api.json", {})
    main.main(["generate", str(api_file), "-"])
    mh.assert_call_argument(
        input_file=api_file, output_file=Path("/dev/stdout"),
        generator_class=HtmlGenerator, generator_config={})


def test_requires_existing_input_file(mh, lg):
    with pytest.raises(SystemExit):
        main.main(["generate", "/does/not/exist"])
    assert "File not found: `/does/not/exist`" in lg.errors


def test_complains_about_unknown_language(mh, capsys):
    with pytest.raises(SystemExit):
        main.main(["generate", "--language", "logban"])
    _out, err = capsys.readouterr()
    assert (
        "argument -l/--language: invalid choice: "
        "'logban' (choose from 'html', 'json', 'yaml')") in err


def test_creates_parent_of_output_file(mh, tmp_path):
    api_file = mh.write("api.json", {})
    output_file = tmp_path / "subdir" / "output.html"
    main.main(['generate', str(api_file), str(output_file)])
    mh.assert_call_argument(
        input_file=api_file, output_file=output_file,
        generator_class=HtmlGenerator)
    assert output_file.parent.exists()


def test_does_not_create_grandparent_of_output_file(mh, tmp_path, lg):
    api_file = mh.write("api.json", {})
    output_file = tmp_path / "subdir" / "evensubier" / "output.html"
    with pytest.raises(SystemExit):
        main.main(['generate', str(api_file), str(output_file)])
    assert f"Output dir does not exist: `{output_file.parent}`" \
        in lg.errors


def test_complains_about_nonexisting_given_config_file(mh, lg):
    api_file = mh.write("api.json", {})
    with pytest.raises(SystemExit):
        main.main(["-c", "/does/not/exist", "generate", str(api_file)])
    assert "File not found: `/does/not/exist`" in lg.errors


def test_reads_config_values_from_config_file(mh):
    api_file = mh.write("api.json", {})
    config_file = mh.write_config(html__key="value")
    main.main(["-c", str(config_file), "generate", str(api_file)])
    mh.assert_call_argument(
        input_file=api_file, output_file=api_file.with_suffix(".html"),
        generator_class=HtmlGenerator,
        generator_config={"key": "value"})


def test_complains_about_config_file_syntax_errors(mh, lg):
    api_file = mh.write("api.json", {})
    config_file = mh.write("config.ini", "no section!")
    with pytest.raises(SystemExit):
        main.main(["-c", str(config_file), "generate", str(api_file)])
    assert (
        "File contains no section headers.\n"
        f"file: '{config_file}', line: 1\n'no section!'") in lg.errors


def test_interprets_config_values_given_on_cli(mh):
    api_file = mh.write("api.json", {})
    main.main(["generate", str(api_file), "-C", "key=value"])
    mh.assert_call_argument(
        input_file=api_file, output_file=api_file.with_suffix(".html"),
        generator_class=HtmlGenerator,
        generator_config={"key": "value"})


@pytest.mark.parametrize("val", ["no_equal_sign", "=value", "key="])
def test_complains_about_invalid_config_values_on_cli(mh, lg, val):
    api_file = mh.write("api.json", {})
    with pytest.raises(SystemExit):
        main.main(["generate", str(api_file), "-C", val])
    assert f"Invalid Key=Value pair: `{val}`" in lg.errors


def test_fails_on_unknown_specs(tmp_path, lg):
    api_file = tmp_path / "api.spec"
    api_file.write_text("Not even a spec")
    with pytest.raises(SystemExit):
        main.main(["generate", str(api_file)])
    assert f"No parser found for file `{api_file}`" in lg.errors


def test_supports_component_extraction(tmp_path):
    spec = minimal_struct()
    spec["paths"]["/go"]["get"]["responses"]["200"]["content"] = {
        "application/json": {"schema": {
            "type": "string"}}}
    api_file = tmp_path / "api.json"
    json.dump(spec, api_file.open("wt"))
    main.main(["generate", str(api_file), "-E", "-l", "yaml"])
    output = yaml.safe_load(api_file.with_suffix(".yaml").open())
    assert "GetResponse" in output["components"]["schemas"]
    content = output["paths"]["/go"]["get"]["responses"][200]["content"]
    assert content["application/json"]["schema"] == {
        "$ref": "#/components/schemas/GetResponse"}
